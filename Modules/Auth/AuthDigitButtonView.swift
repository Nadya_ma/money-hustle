//
//  AuthDigitButtonView.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 15.10.2021.
//

import UIKit

final class AuthDigitButtonView: UIButton {
    
    var digit: Int? {
        didSet {
            if let digit = digit {
                setTitleColor(Theme.current.mainTextColor, for: .normal)
                setTitle("\(digit)", for: .normal)
                tag = digit
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setTitleColor(Theme.current.mainTextColor, for: .normal)
        layer.borderColor = Theme.current.separatorColor.cgColor
    }
    
    private func setupViews() {
        layer.borderColor = Theme.current.separatorColor.cgColor
        layer.borderWidth = 2
        layer.cornerRadius = 24
        snp.makeConstraints { $0.size.equalTo(48) }
    }
}
