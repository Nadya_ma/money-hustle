//
//  AuthPresenter.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 31.08.2021.
//

import Foundation
import LocalAuthentication

protocol IAuthDelegate: AnyObject {
    func lockStateUpdated()
}

protocol IAuthPresenter: IBasePresenter {
    var isBiometricAuth: Bool { get }
    
    func viewBackTapped()
    func saveTapped(code: String?)
    func deleteCodeTapped()
    func biometricAuth()
}


final class AuthPresenter: IAuthPresenter {
    private let wireframe: IAuthWireframe
    private weak var delegate: IAuthDelegate?
    private weak var view: IAuthViewController?
    
    private let context = LAContext()
    private var cachedCode: String?
    var isBiometricAuth: Bool { // check if it's auth process
        !sessionManager.isAuth && // check if user's auth-d
        context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
    }
    
    init(wireframe: IAuthWireframe, delegate: IAuthDelegate?, view: IAuthViewController) {
        self.wireframe = wireframe
        self.delegate = delegate
        self.view = view
    }
    
    func viewDidLoad() {
        if sessionManager.isAuth {
            view?.setTitle(title: "Set code".localized(), buttonTitle: "Set".localized())
            view?.hideDeleteButton(!sessionManager.isCodeSet)
            if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
                view?.setfaceTouchIdInfo("FaceID/TouchID enabled. You can disable it in your device settings".localized())
            } else {
                view?.setfaceTouchIdInfo("FaceID/TouchID disabled. You can enable it in your device settings".localized())
            }
        } else {
            view?.isSafeView = true
            view?.hideDeleteButton(true)
            view?.setTitle(title: "Enter code".localized(), buttonTitle: "Enter".localized())
            view?.setfaceTouchIdInfo(nil)
            
            biometricAuth()
        }
    }
    
    func viewBackTapped() {
        wireframe.closeModule()
    }
    
    func saveTapped(code: String?) {
        if sessionManager.isAuth {
            guard let code = code, !code.isEmpty else {
                view?.showError(message: "Code cannot be empty".localized())
                return
            }
            
            if cachedCode == nil {
                cachedCode = code
                view?.setTitle(title: "Repeat code".localized(), buttonTitle: "Enter".localized())
            } else {
                guard let cachedCode = cachedCode, cachedCode == code else {
                    view?.showError(message: "Codes are not same".localized())
                    return
                }
                sessionManager.setEnterCode(code: code)
                delegate?.lockStateUpdated()
                wireframe.closeModule()
            }
        } else {
            guard let code = code, !code.isEmpty else {
                view?.showError(message: "Please enter passcode".localized())
                return
            }
            
            sessionManager.isCorrectCode(enteredCode: code) ? wireframe.closeModule() : view?.showError(message: "Code is wrong".localized())
        }
    }
    
    func deleteCodeTapped() {
        wireframe.presentConfirmationAlert(question: "Do you want to enter without code?".localized(), okHandler: { [weak self] _ in
            self?.sessionManager.deleteEnterCode()
            self?.delegate?.lockStateUpdated()
            self?.wireframe.closeModule()
        }, cancelHandler: nil)
    }
    
    func biometricAuth() {
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics,
                                   localizedReason: "Access with id".localized()) { [weak self] (success, _) in
                if success {
                    DispatchQueue.main.async { [weak self] in
                        self?.sessionManager.safeEntrance()
                        self?.wireframe.closeModule()
                    }
                }
            }
        } else {
            view?.setfaceTouchIdInfo("FaceID/TouchID disabled. You can enable it in your device settings".localized())
        }
    }
}
