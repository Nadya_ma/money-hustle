//
//  AuthViewController.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 31.08.2021.
//

import UIKit

protocol IAuthViewController: IBaseViewController {
    var presenter: IAuthPresenter? { get set }
    var isSafeView: Bool { get set }
    
    func setTitle(title: String, buttonTitle: String)
    func showError(message: String)
    func hideDeleteButton(_ isHidden: Bool)
    func setfaceTouchIdInfo(_ info: String?)
}


final class AuthViewController: BaseViewController {
    var presenter: IAuthPresenter?
    
    private lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.alwaysBounceVertical = true
        view.alwaysBounceHorizontal = false
        view.backgroundColor = .clear
        view.showsVerticalScrollIndicator = false
        return view
    }()
    private lazy var mainContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    private let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Const.bigCornerRadius
        view.isUserInteractionEnabled = true
        return view
    }()
    private let blurEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .regular)
        let view = UIVisualEffectView(effect: blurEffect)
        view.alpha = 0.9
        return view
    }()
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Set code".localized()
        return label
    }()
    private lazy var digitsLabel: UILabel = {
        let label = UILabel()
        label.font = Const.h1BoldTitleFont
        return label
    }()
    private lazy var digitsStack: UIStackView = {
        let stack = UIStackView()
        stack.distribution = .equalSpacing
        stack.axis = .vertical
        stack.spacing = 24
        stack.addArrangedSubview(makeDigitsStack(for: 1...3))
        stack.addArrangedSubview(makeDigitsStack(for: 4...6))
        stack.addArrangedSubview(makeDigitsStack(for: 7...9))
        stack.addArrangedSubview(makeLastDigitsStack())
        return stack
    }()
    private let errorLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = Const.addDetailsFont
        return label
    }()
    private lazy var biometricAuthButton: UIButton = {
        let button = UIButton()
        button.layer.borderWidth = 2
        button.layer.cornerRadius = 24
        button.snp.makeConstraints { $0.size.equalTo(48) }
        button.setImage(UIImage(systemName: "faceid"), for: .normal)
        button.addTarget(self, action: #selector(biometricAuthTapped), for: .touchUpInside)
        return button
    }()
    private lazy var backSpaceButton: UIButton = {
        let button = UIButton()
        button.layer.borderWidth = 2
        button.layer.cornerRadius = 24
        button.snp.makeConstraints { $0.size.equalTo(48) }
        button.setTitle("⌫", for: .normal)
        button.addTarget(self, action: #selector(buttonBackTapped), for: .touchUpInside)
        return button
    }()
    private let button: UIButton = {
        var button = UIButton()
        button.setTitle("Set".localized(), for: .normal)
        button.isEnabled = false
        button.layer.cornerRadius = Const.cornerRadius
        button.addTarget(self, action: #selector(setTapped), for: .touchUpInside)
        return button
    }()
    private let faceTouchIdInfoLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = Const.descriptionFont
        return label
    }()
    private let deleteButton: UIButton = {
        var button = UIButton()
        button.setTitle("Enter without code".localized(), for: .normal)
        button.setTitleColor(Theme.current.minusTextColor, for: .normal)
        button.addTarget(self, action: #selector(deleteTapped), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    var isSafeView: Bool = false {
        didSet {
            blurEffectView.alpha = isSafeView ? 1 : 0.5
        }
    }
    private var code: String = "" {
        didSet {
            if !code.isEmpty {
                button.isEnabled = code.count == 4
                button.backgroundColor = code.count == 4 ? Theme.current.separatorColor : Theme.current.secondBackgroundColor
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subscribeToShowKeyboardNotifications()
        presenter?.viewDidLoad()
    }
    
    override func setupAppearance() {
        titleLabel.textColor = Theme.current.mainTextColor
        containerView.backgroundColor = Theme.current.mainBackgroundColor
        digitsLabel.textColor = Theme.current.mainTextColor
        backSpaceButton.layer.borderColor = Theme.current.separatorColor.cgColor
        backSpaceButton.setTitleColor(Theme.current.mainTextColor, for: .normal)
        biometricAuthButton.layer.borderColor = Theme.current.separatorColor.cgColor
        biometricAuthButton.tintColor = Theme.current.mainTextColor
        button.backgroundColor = code.count == 4 ? Theme.current.separatorColor : Theme.current.secondBackgroundColor
        faceTouchIdInfoLabel.textColor = Theme.current.mainTextColor
    }
    
    override func setupViews() {
        view.backgroundColor = .black.withAlphaComponent(0.3)
        setupAppearance()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:))))
        
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.insertSubview(blurEffectView, at: 0)
        
        let containerWidth = min(view.frame.width, view.frame.height)
        
        view.addSubview(scrollView)
        scrollView.snp.remakeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        scrollView.addSubview(mainContainerView)
        mainContainerView.snp.remakeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalTo(view.snp.width)
        }

        mainContainerView.addSubview(containerView)
        containerView.snp.remakeConstraints { make in
            make.width.equalTo(containerWidth * 0.67)
            make.top.equalToSuperview().offset(120)
            make.centerX.equalToSuperview()
        }
        
        containerView.addSubview(titleLabel)
        titleLabel.snp.remakeConstraints { make in
            make.top.leading.trailing.equalToSuperview().inset(Const.largeOffset)
        }
        
        containerView.addSubview(digitsLabel)
        digitsLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(Const.largeOffset)
            make.centerX.equalToSuperview()
            make.height.equalTo(30)
        }
        
        containerView.addSubview(digitsStack)
        digitsStack.snp.remakeConstraints { make in
            make.top.equalTo(digitsLabel.snp.bottom).offset(Const.largeOffset)
            make.centerX.equalToSuperview()
            make.width.equalTo(24*2 + 48*3)
        }
        
        containerView.addSubview(errorLabel)
        errorLabel.snp.makeConstraints { make in
            make.top.equalTo(digitsStack.snp.bottom).offset(Const.mainOffset)
            make.leading.trailing.equalToSuperview().inset(Const.largeOffset)
        }
        
        containerView.addSubview(button)
        button.snp.remakeConstraints { make in
            make.top.equalTo(errorLabel.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.equalToSuperview().inset(Const.largeOffset)
            make.height.equalTo(Const.buttonHeight)
        }
        
        containerView.addSubview(faceTouchIdInfoLabel)
        faceTouchIdInfoLabel.snp.makeConstraints { make in
            make.top.equalTo(button.snp.bottom).offset(Const.mainOffset)
            make.leading.trailing.bottom.equalToSuperview().inset(Const.largeOffset)
        }
        
        mainContainerView.addSubview(deleteButton)
        deleteButton.snp.remakeConstraints { make in
            make.top.equalTo(containerView.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.equalTo(containerView)
            make.height.equalTo(Const.buttonHeight)
            make.bottom.equalToSuperview()
        }
    }
    
    override func setupTransitionConstraints() {
        setupViews()
    }
    
    @objc private func buttonTapped(_ button: UIButton) {
        if code.count < 4 {
            let enteringChar = isSafeView ? "*" : "\(button.tag)"
            let text = digitsLabel.text != nil ? digitsLabel.text! + "  \(enteringChar)" : "\(enteringChar)"
            digitsLabel.text = text
            code.append(String(button.tag))
            
            print(code)
        }
    }
    
    @objc private func setTapped() {
        presenter?.saveTapped(code: code)
    }
    
    @objc private func buttonBackTapped() {
        if var text = digitsLabel.text, !text.isEmpty {
            text.removeLast(text.count > 3 ? 3 : 1)
            digitsLabel.text = text
            if !code.isEmpty {
                code.removeLast()
            }
            
            print(code)
        }
    }
    
    @objc private func deleteTapped() {
        presenter?.deleteCodeTapped()
    }
    
    @objc func viewTapped(_ gr: UITapGestureRecognizer) {
        if !isSafeView {
            let point = gr.location(in: gr.view)
            if !containerView.frame.contains(point) {
                presenter?.viewBackTapped()
            }
        }
    }
    
    @objc private func biometricAuthTapped() {
        presenter?.biometricAuth()
    }
    
    private func makeDigitsStack(for range: ClosedRange<Int>) -> UIStackView {
        let stack = UIStackView()
        stack.distribution = .equalSpacing
        stack.axis = .horizontal
        range.forEach { stack.addArrangedSubview(makeDigitButton(for: $0)) }
        return stack
    }
    
    private func makeLastDigitsStack() -> UIStackView {
        let stack = UIStackView()
        stack.distribution = .equalSpacing
        stack.axis = .horizontal
        if presenter?.isBiometricAuth ?? false {
            stack.addArrangedSubview(biometricAuthButton)
        } else {
            let gapView = UIView()
            gapView.snp.makeConstraints { $0.size.equalTo(48) }
            stack.addArrangedSubview(gapView)
        }
        stack.addArrangedSubview(makeDigitButton(for: 0))
        stack.addArrangedSubview(backSpaceButton)
        return stack
    }
    
    private func makeDigitButton(for digit: Int) -> UIButton {
        let button = AuthDigitButtonView()
        button.digit = digit
        button.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
        return button
    }
}


extension AuthViewController {
    private func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 142 // 128 xs
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        scrollView.contentInset.bottom = 0
    }
}

extension AuthViewController: IAuthViewController {
    func setTitle(title: String, buttonTitle: String) {
        titleLabel.text = title
        button.setTitle(buttonTitle, for: .normal)
        code = ""
        digitsLabel.text = ""
        errorLabel.text = nil
    }
    
    func showError(message: String) {
        errorLabel.text = message
    }
    
    func hideDeleteButton(_ isHidden: Bool) {
        deleteButton.isHidden = isHidden
    }
    
    func setfaceTouchIdInfo(_ info: String?) {
        faceTouchIdInfoLabel.text = info
    }
}
