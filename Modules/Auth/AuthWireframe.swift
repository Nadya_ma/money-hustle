//
//  AuthWireframe.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 31.08.2021.
//

import UIKit

protocol IAuthWireframe: IBaseWireframe {
    func closeModule()
}

final class AuthWireframe: IAuthWireframe {
    var view: IBaseViewController?
    
    init(view: IBaseViewController) {
        self.view = view
    }
    
    static func buildModule(delegate: IAuthDelegate?) -> UIViewController {
        let view: IAuthViewController & UIViewController = AuthViewController()
        let wireframe: IAuthWireframe = AuthWireframe(view: view)
        let presenter: IAuthPresenter = AuthPresenter(wireframe: wireframe, delegate: delegate, view: view)
        view.presenter = presenter
        return view
    }
    
    func closeModule() {
        view?.dismiss(animated: true, completion: nil)
    }
}
