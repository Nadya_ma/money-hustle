//
//  BasePresenter.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 29.07.2021.
//

import Foundation
import CoreData

protocol IBasePresenter: AnyObject {
    
    var coreDataManager: ICoreDataManager { get }
    var sessionManager: ISessionManager { get }
    
    func viewDidLoad()
}

extension IBasePresenter {
    
    var coreDataManager: ICoreDataManager {
        CoreDataManager.shared
    }
    
    var sessionManager: ISessionManager {
        SessionManager.shared
    }
}
