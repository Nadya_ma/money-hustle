//
//  BaseViewController.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 29.07.2021.
//

import UIKit

protocol IBaseViewController where Self: BaseViewController {
    
    func showLoading()
    func hideLoading()
}

extension IBaseViewController {
    func showLoading() {}
    func hideLoading() {}
}


class BaseViewController: UIViewController {
    
    var navBarHeight: CGFloat = 0.0
    var safeNavBarHeight: CGFloat = 0.0
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navBarHeight = navigationController?.navigationBar.frame.height ?? 0
        safeNavBarHeight = navBarHeight + 38
        setupViews()
        
        switch traitCollection.userInterfaceStyle {
        case .light, .unspecified: Theme.current = .light
        case .dark: Theme.current = .dark
        @unknown default: return
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        switch traitCollection.userInterfaceStyle {
        case .light, .unspecified: Theme.current = .light
        case .dark: Theme.current = .dark
        @unknown default: return
        }
        setupAppearance()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        setupTransitionConstraints()
    }
    
    func setupViews() {}
    
    func setupAppearance() {}
    
    func setupTransitionConstraints() {}
}
