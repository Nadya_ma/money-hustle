//
//  BaseWireframe.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 29.07.2021.
//

import UIKit

protocol IBaseWireframe: AnyObject {
    var view: IBaseViewController? { get set }
    
    func presentConfirmationAlert(question: String, okHandler: ((UIAlertAction) -> Void)?, cancelHandler: ((UIAlertAction) -> Void)?)
}

extension IBaseWireframe {
    func presentConfirmationAlert(question: String, okHandler: ((UIAlertAction) -> Void)?, cancelHandler: ((UIAlertAction) -> Void)?) {
        let dialogMessage = UIAlertController(title: "Confirm".localized(), message: question, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok".localized(), style: .default, handler: okHandler)
        let cancel = UIAlertAction(title: "Cancel".localized(), style: .default, handler: cancelHandler)
        
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        
        view?.present(dialogMessage, animated: true, completion: nil)
    }
}
