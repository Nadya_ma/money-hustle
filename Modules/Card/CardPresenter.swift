//
//  CardPresenter.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 23.08.2021.
//

import Foundation

protocol ICardOutput: AnyObject {
    func cardEdited()
    func cardDeleted()
}

typealias ICardCategoryOutput = ICategoryOutput & ICardOutput & ITransferEditorOutput

protocol ICardPresenter: IBasePresenter {
    var card: Card { get }
    var cardOutcomes: [CardOutcome] { get }
    
    func onEditTapped()
    func onOutcomeTapped(at index: Int)
}


final class CardPresenter: ICardPresenter {
    private let wireframe: ICardWireframe
    private weak var view: ICardViewController?
    private weak var delegate: ICardCategoryOutput?
    
    var card: Card
    private let period: Period
    
    var cardOutcomes: [CardOutcome] {
        calculateCardOutcomes()
    }
    
    init(wireframe: ICardWireframe, view: ICardViewController, delegate: ICardCategoryOutput, card: Card, period: Period) {
        self.wireframe = wireframe
        self.view = view
        self.delegate = delegate
        self.card = card
        self.period = period
    }
    
    
    func viewDidLoad() {
        view?.setCard(card)
        view?.setOutcomes()
    }
    
    private func calculateCardOutcomes() -> [CardOutcome] {
        var outcomes = [CardOutcome]()
        
        coreDataManager.getObjects(of: Category.self).forEach { category in
            let categoryOutcomes = coreDataManager.getOutcomes(for: period).filter { $0.card_id == card.id && $0.category_id == category.id }
            
            let date = categoryOutcomes.last?.date ?? Date()
            let amount = categoryOutcomes.reduce(0.0) { $0 + $1.spent }
            
            if amount > 0 {
                outcomes.append(CardOutcome(date: date, type: .outcome, category: category, card: nil, amount: -amount))
            }
        }
        
        let transfers = coreDataManager.getTransfers(for: period).filter { $0.from_card_id == card.id || $0.to_card_id == card.id }
        transfers.forEach { transfer in
            if transfer.from_card_id == card.id {
                if transfer.to_card_id == card.id {
                    outcomes.append(CardOutcome(date: transfer.date, type: .changeSum, category: nil, transfer: transfer, card: card, amount: transfer.amount))
                } else {
                    let card = coreDataManager.getObject(of: Card.self, id: transfer.to_card_id)
                    outcomes.append(CardOutcome(date: transfer.date, type: .transfer, category: nil, transfer: transfer, card: card, amount: -transfer.amount))
                }
            } else if transfer.to_card_id == card.id {
                let card = coreDataManager.getObject(of: Card.self, id: transfer.from_card_id)
                outcomes.append(CardOutcome(date: transfer.date, type: .transfer, category: nil, transfer: transfer, card: card, amount: +transfer.amount))
            }
        }
        
        return outcomes.sorted { $0.amount < $1.amount }
    }
    
    func onEditTapped() {
        wireframe.presentCardEditorView(card: card, delegate: self)
    }
    
    func onOutcomeTapped(at index: Int) {
        if index < cardOutcomes.count {
            let outcome = cardOutcomes[index]
            switch outcome.type {
            case .outcome:
                guard let category = outcome.category else { return }
                wireframe.moveToCategoryView(category: category, period: period, delegate: self, card: card)
            case .changeSum, .transfer:
                guard let transfer = outcome.transfer else { return }
                wireframe.presentTransferEditorView(transfer: transfer, delegate: self)
            }
        }
    }
    
    private func reloadCard() {
        if let changedCard = coreDataManager.getObject(of: Card.self, id: card.id) {
            card = changedCard
        }
    }
}


extension CardPresenter: ICardEditorOutput {
    func cardEdited() {
        reloadCard()
        viewDidLoad()
        delegate?.cardEdited()
    }
    
    func cardDeleted() {
        delegate?.cardDeleted()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) { [weak self] in
            self?.wireframe.closeModule()
        }
    }
}

extension CardPresenter: ICategoryOutput {
    func categoryEdited() {
        reloadCard()
        viewDidLoad()
        delegate?.categoryEdited()
    }
    
    func categoryDeleted() {
        reloadCard()
        viewDidLoad()
        delegate?.categoryDeleted()
    }
    
    func outcomeEdited() {
        reloadCard()
        viewDidLoad()
        delegate?.outcomeEdited()
    }
}

extension CardPresenter: ITransferEditorOutput {
    func transferEdited() {
        reloadCard()
        viewDidLoad()
        delegate?.transferEdited()
    }
}
