//
//  CardViewController.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 23.08.2021.
//

import UIKit

protocol ICardViewController: IBaseViewController {
    var presenter: ICardPresenter? { get set }
    
    func setCard(_ card: Card)
    func setOutcomes()
}


final class CardViewController: BaseViewController {
    var presenter: ICardPresenter?
    
    private lazy var outcomesTable: UITableView = {
        let tv = UITableView()
        tv.register(CardOutcomeCell.self, forCellReuseIdentifier: CardOutcomeCell.id)
        tv.estimatedRowHeight = 200
        tv.dataSource = self
        tv.delegate = self
        tv.separatorStyle = .none
        return tv
    }()
    private var balanceLabel: UILabel = {
        let label = UILabel()
        label.font = Const.h1BoldTitleFont
        label.textAlignment = .center
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func setupViews() {
        setupAppearance()
        
        let button = UIBarButtonItem(title: "Edit".localized(), style: .plain, target: self, action: #selector(changeTapped))
        navigationItem.rightBarButtonItem = button
        let backButton = UIBarButtonItem()
        backButton.title = "Back".localized()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        view.addSubview(outcomesTable)
        outcomesTable.snp.makeConstraints { make in
            make.top.equalTo(view).offset(navBarHeight)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(-view.safeAreaInsets.bottom - Const.mainOffset)
        }
    }
    
    override func setupAppearance() {
        view.backgroundColor = Theme.current.secondBackgroundColor
        balanceLabel.textColor = Theme.current.mainTextColor
        outcomesTable.backgroundColor = Theme.current.secondBackgroundColor
        outcomesTable.reloadData()
    }
    
    @objc private func changeTapped() {
        presenter?.onEditTapped()
    }
}


extension CardViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter?.cardOutcomes.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CardOutcomeCell.id, for: indexPath)
        if let presenter = presenter, indexPath.row < presenter.cardOutcomes.count {
            (cell as? CardOutcomeCell)?.cardOutcome = presenter.cardOutcomes[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let mainView = UIView()
        
        let colorIndex: Int = Int(presenter?.card.id ?? 0) % cardColors.count
        mainView.backgroundColor = cardColors[colorIndex].withAlphaComponent(0.5)
        
        let balanceTitleLabel = UILabel()
        balanceTitleLabel.font = Const.descriptionFont
        balanceTitleLabel.textColor = Theme.current.mainTextColor
        balanceTitleLabel.textAlignment = .center
        balanceTitleLabel.text = "Balance".localized()
    
        mainView.addSubview(balanceTitleLabel)
        balanceTitleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(Const.largeOffset)
            make.centerX.equalToSuperview()
        }
        
        mainView.addSubview(balanceLabel)
        balanceLabel.snp.makeConstraints { make in
            make.top.equalTo(balanceTitleLabel.snp.bottom)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-Const.largeOffset)
        }
        return mainView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = Theme.current.secondBackgroundColor
        
        let incomeLabel = UILabel()
        incomeLabel.textAlignment = .right
        incomeLabel.font = Const.boldTextFont
        incomeLabel.textColor = Theme.current.mainTextColor
        
        let finalLabel = UILabel()
        finalLabel.textAlignment = .right
        finalLabel.font = Const.boldTextFont
        finalLabel.textColor = Theme.current.mainTextColor
        
        if let presenter = presenter {
            let incomeAmount = presenter.cardOutcomes.reduce(0) { $0 + ($1.amount > 0 ? $1.amount : 0) }
            incomeLabel.text = "Total income".localized() + ": " + incomeAmount.stringValue
            let finalAmount = presenter.cardOutcomes.reduce(0) { $0 + ($1.amount < 0 ? $1.amount : 0) }
            finalLabel.text = "Total spent".localized() + ": " + finalAmount.stringValue
        }
        
        view.addSubview(incomeLabel)
        incomeLabel.snp.makeConstraints { make in
            make.top.trailing.equalToSuperview().inset(Const.largeOffset)
        }
        
        view.addSubview(finalLabel)
        finalLabel.snp.makeConstraints { make in
            make.top.equalTo(incomeLabel.snp.bottom)
            make.trailing.bottom.equalToSuperview().inset(Const.largeOffset)
        }
        
        return view
    }
}

extension CardViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.onOutcomeTapped(at: indexPath.row)
    }
}

extension CardViewController: ICardViewController {
    func setCard(_ card: Card) {
        title = card.name
        balanceLabel.text = card.amount.stringValue
    }
    
    func setOutcomes() {
        outcomesTable.reloadData()
    }
}

