//
//  CardWireframe.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 23.08.2021.
//

import UIKit

protocol ICardWireframe: IBaseWireframe {
    func presentCardEditorView(card: Card, delegate: ICardEditorOutput)
    func moveToCategoryView(category: Category, period: Period, delegate: ICategoryOutput, card: Card)
    func presentTransferEditorView(transfer: Transfer, delegate: ITransferEditorOutput?)
    func closeModule()
}


final class CardWireframe: ICardWireframe {
    var view: IBaseViewController?
    
    init(view: IBaseViewController) {
        self.view = view
    }
    
    static func buildModule(card: Card, period: Period, delegate: ICardCategoryOutput) -> UIViewController {
        let view: ICardViewController = CardViewController()
        let wireframe: ICardWireframe = CardWireframe(view: view)
        let presenter: ICardPresenter = CardPresenter(wireframe: wireframe, view: view, delegate: delegate, card: card, period: period)
        view.presenter = presenter
        return view
    }
    
    func presentCardEditorView(card: Card, delegate: ICardEditorOutput) {
        let vc = CardEditorWireframe.buildModule(card: card, delegate: delegate) as UIViewController
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        view?.present(vc, animated: true, completion: nil)
    }
    
    func moveToCategoryView(category: Category, period: Period, delegate: ICategoryOutput, card: Card) {
        let vc = CategoryWireframe.buildModule(category: category, delegate: delegate, period: period, card: card)
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func presentTransferEditorView(transfer: Transfer, delegate: ITransferEditorOutput?) {
        let vc = TransferEditorWireframe.buildModule(from: nil, to: nil, delegate: delegate, transfer: transfer) as UIViewController
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        view?.present(vc, animated: true, completion: nil)
    }
    
    func closeModule() {
        view?.navigationController?.popViewController(animated: true)
    }
}
