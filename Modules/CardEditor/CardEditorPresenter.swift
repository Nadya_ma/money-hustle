//
//  CardEditorPresenter.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 11.08.2021.
//

import Foundation

protocol ICardEditorOutput: AnyObject {
    func cardEdited()
    func cardDeleted()
}

protocol ICardEditorPresenter: IBasePresenter {
    func viewBackTapped()
    func saveTapped(name: String, value: String)
    func deleteTapped()
}


final class CardEditorPresenter: ICardEditorPresenter {
    private let wireframe: ICardEditorWireframe
    private weak var view: ICardEditorViewController?
    private weak var delegate: ICardEditorOutput?
    
    private var card: Card?
    
    init(wireframe: ICardEditorWireframe, view: ICardEditorViewController, delegate: ICardEditorOutput?, card: Card?) {
        self.wireframe = wireframe
        self.view = view
        self.delegate = delegate
        self.card = card
    }
    
    
    func viewDidLoad() {
        if card != nil {
            view?.setCard(card: card!)
        }
    }
    
    func viewBackTapped() {
        wireframe.closeModule()
    }
    
    func saveTapped(name: String, value: String) {
        let enteredAmount = Double(value) ?? 0
        if let card = card {
            let cardAmount = card.amount
            card.name = name
            card.amount = enteredAmount
            
            let transfer = coreDataManager.createObject(of: Transfer.self)
            transfer.configure(fromCard: card, toCard: card, amount: enteredAmount - cardAmount, date: Date())
        } else {
            let newCard = coreDataManager.createObject(of: Card.self)
            newCard.configure(name: name, amount: enteredAmount)
        }
        coreDataManager.saveChanges()
        delegate?.cardEdited()
        wireframe.closeModule()
    }
    
    func deleteTapped() {
        wireframe.presentConfirmationAlert(question: "Are you sure you want to delete this?".localized(), okHandler: { [weak self] _ in
            guard  let card = self?.card else { return }
            card.isArchived = true
            self?.coreDataManager.saveChanges()
            self?.delegate?.cardDeleted()
            self?.wireframe.closeModule()
        }, cancelHandler: nil)
    }
}
