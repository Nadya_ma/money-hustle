//
//  CardEditorViewController.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 11.08.2021.
//

import UIKit

protocol ICardEditorViewController: IBaseViewController {
    var presenter: ICardEditorPresenter? { get set }
    
    func setCard(card: Card)
}


final class CardEditorViewController: BaseViewController {
    var presenter: ICardEditorPresenter?
    
    private lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.alwaysBounceVertical = true
        view.alwaysBounceHorizontal = false
        view.backgroundColor = .clear
        view.showsVerticalScrollIndicator = false
        return view
    }()
    private lazy var mainContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    private let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Const.bigCornerRadius
        view.isUserInteractionEnabled = true
        return view
    }()
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "New card".localized()
        return label
    }()
    private let nameField: CommonTextField = {
        let field = CommonTextField()
        field.title = "Name".localized()
        return field
    }()
    private let balanceField: CommonAmountTextField = {
        let field = CommonAmountTextField()
        field.title = "Initial balance".localized()
        return field
    }()
    private let saveButton: UIButton = {
        var button = UIButton()
        button.setTitle("Save".localized(), for: .normal)
        button.layer.cornerRadius = Const.cornerRadius
        button.addTarget(self, action: #selector(saveTapped), for: .touchUpInside)
        return button
    }()
    private let deleteButton: UIButton = {
        var button = UIButton()
        button.setTitle("Delete".localized(), for: .normal)
        button.addTarget(self, action: #selector(deleteTapped), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subscribeToShowKeyboardNotifications()
        presenter?.viewDidLoad()
    }
    
    override func setupViews() {
        view.backgroundColor = .black.withAlphaComponent(0.3)
        setupAppearance()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:))))
        
        let containerWidth = min(view.frame.width, view.frame.height)
        
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        scrollView.addSubview(mainContainerView)
        mainContainerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalTo(view.snp.width)
        }

        mainContainerView.addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.width.equalTo(containerWidth * 0.67)
            make.top.equalToSuperview().offset(120)
            make.centerX.equalToSuperview()
        }
        
        containerView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview().inset(Const.largeOffset)
        }
        
        containerView.addSubview(nameField)
        nameField.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.equalToSuperview().inset(Const.largeOffset)
            make.height.equalTo(2*Const.iconCvItemSize.height)
        }
        
        containerView.addSubview(balanceField)
        balanceField.snp.makeConstraints { make in
            make.top.equalTo(nameField.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.equalToSuperview().inset(Const.largeOffset)
            make.height.equalTo(2*Const.iconCvItemSize.height)
        }
        
        containerView.addSubview(saveButton)
        saveButton.snp.makeConstraints { make in
            make.top.equalTo(balanceField.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.bottom.equalToSuperview().inset(Const.largeOffset)
            make.height.equalTo(Const.buttonHeight)
        }
        
        mainContainerView.addSubview(deleteButton)
        deleteButton.snp.makeConstraints { make in
            make.top.equalTo(containerView.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.equalTo(containerView)
            make.height.equalTo(Const.buttonHeight)
            make.bottom.equalToSuperview()
        }
    }
    
    override func setupAppearance() {
        containerView.backgroundColor = Theme.current.mainBackgroundColor
        titleLabel.textColor = Theme.current.mainTextColor
        saveButton.backgroundColor = Theme.current.separatorColor
        deleteButton.setTitleColor(Theme.current.minusTextColor, for: .normal)
    }
    
    @objc func viewTapped(_ gr: UITapGestureRecognizer) {
        let point = gr.location(in: gr.view)
        if !containerView.frame.contains(point) {
            presenter?.viewBackTapped()
        }
    }
    
    @objc func saveTapped() {
        guard let name = nameField.value, let value = balanceField.value else { return }
        presenter?.saveTapped(name: name, value: value)
    }
    
    @objc func deleteTapped() {
        presenter?.deleteTapped()
    }
}


extension CardEditorViewController: ICardEditorViewController {
    func setCard(card: Card) {
        titleLabel.text = "Edit card".localized()
        nameField.value = card.name
        balanceField.value = String(card.amount)
        deleteButton.isHidden = false
    }
}

extension CardEditorViewController {
    private func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 142 // 128 xs
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        scrollView.contentInset.bottom = 0
    }
}
