//
//  CardEditorWireframe.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 11.08.2021.
//

import UIKit

protocol ICardEditorWireframe: IBaseWireframe {
    func closeModule()
}

final class CardEditorWireframe: ICardEditorWireframe {
    weak var view: IBaseViewController?
    
    init(view: IBaseViewController) {
        self.view = view
    }
    
    static func buildModule(card: Card?, delegate: ICardEditorOutput) -> UIViewController {
        let view: ICardEditorViewController = CardEditorViewController()
        let wireframe: ICardEditorWireframe = CardEditorWireframe(view: view)
        let presenter: ICardEditorPresenter = CardEditorPresenter(wireframe: wireframe, view: view, delegate: delegate, card: card)
        view.presenter = presenter
        return view
    }
    
    func closeModule() {
        view?.dismiss(animated: true, completion: nil)
    }
}
