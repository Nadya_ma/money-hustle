//
//  CategoryPresenter.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 16.08.2021.
//

import Foundation

protocol ICategoryOutput: AnyObject {
    func categoryEdited()
    func categoryDeleted()
    func outcomeEdited()
}

protocol ICategoryPresenter: IBasePresenter {
    var categoryOutcomes: [Outcome] { get }
    var categoryStat: CategoryStatistics { get }
    func onEditTapped()
    func onOutcomeTapped(at index: Int)
}


final class CategoryPresenter: ICategoryPresenter {
    private let wireframe: ICategoryWireframe
    private weak var view: ICategoryViewController?
    private weak var delegate: ICategoryOutput?
    
    private var category: Category
    private var period: Period
    private let periodOffset: Int
    private let card: Card?
    var categoryOutcomes: [Outcome] {
        let startDate = period.getStartDate(offset: periodOffset)
        let endDate = period.getEndDate(offset: periodOffset)
        let outcomes = coreDataManager.getOutcomes(startDate: startDate, endDate: endDate)
        let categoryOutcomes = outcomes.filter { $0.category_id == category.id && ( card == nil ? true : $0.card_id == card!.id ) }
        return categoryOutcomes.sorted { $0.date > $1.date }
    }
    var categoryStat: CategoryStatistics {
        let mth = Period.init(value: .month, isChoosen: false)
        let startDate = mth.getStartDate(offset: periodOffset)
        let endDate = mth.getEndDate(offset: periodOffset)
        let mthOutcomes = coreDataManager.getOutcomes(startDate: startDate, endDate: endDate)
        let categoryMthOutcomes = mthOutcomes.filter { $0.category_id == category.id && ( card == nil ? true : $0.card_id == card!.id ) }
        let mthSpent = categoryMthOutcomes.reduce(0) { $0 + $1.spent }
        let periodSpent = categoryOutcomes.reduce(0) { $0 + $1.spent }
        return CategoryStatistics(of: category, mthSpent: mthSpent, for: period, periodSpent: periodSpent)
    }
    
    init(wireframe: ICategoryWireframe, view: ICategoryViewController, delegate: ICategoryOutput, category: Category, period: Period, periodOffset: Int = 0, card: Card? = nil) {
        self.wireframe = wireframe
        self.view = view
        self.delegate = delegate
        self.category = category
        self.period = period
        self.periodOffset = periodOffset
        self.card = card
    }
    
    
    func viewDidLoad() {
        view?.setCategory(categoryStat)
        view?.setOutcomes()
    }
    
    func onEditTapped() {
        wireframe.presentCategoryEditorView(category: category, delegate: self)
    }
}


extension CategoryPresenter: ICategoryEditorOutput {
    func categoryDeleted() {
        delegate?.categoryDeleted()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) { [weak self] in
            self?.wireframe.closeModule()
        }
    }
    
    func categoryEdited() {
        if let newCategory = coreDataManager.getObject(of: Category.self, id: category.id) {
            category = newCategory
            view?.setCategory(categoryStat)
            delegate?.categoryEdited()
        }
    }
    
    func onOutcomeTapped(at index: Int) {
        if index < categoryOutcomes.count {
            wireframe.presentOutcomeEditorView(outcome: categoryOutcomes[index], delegate: self)
        }
    }
}

extension CategoryPresenter: IOutcomeEditorOutput {
    func outcomeEdited() {
        view?.setOutcomes()
        delegate?.outcomeEdited()
    }
}
