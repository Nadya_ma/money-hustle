//
//  CategoryViewController.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 16.08.2021.
//

import UIKit

protocol ICategoryViewController: IBaseViewController {
    var presenter: ICategoryPresenter? { get set }
    
    func setCategory(_ categoryStat: CategoryStatistics)
    func setOutcomes()
}


final class CategoryViewController: BaseViewController {
    var presenter: ICategoryPresenter?
    
    private let planView = UIView()
    private var planIcon: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        return view
    }()
    private var planTitleLabel: UILabel = {
        let label = UILabel()
        label.font = Const.descriptionFont
        label.textAlignment = .center
        label.text = "Monthly plan".localized()
        return label
    }()
    private var planValueLabel: UILabel = {
        let label = UILabel()
        label.font = Const.h1BoldTitleFont
        label.textAlignment = .center
        return label
    }()
    private var periodPlanLabel: UILabel = {
        let label = UILabel()
        label.font = Const.boldTextFont
        label.textAlignment = .right
        return label
    }()
    private lazy var outcomesTable: UITableView = {
        let tv = UITableView()
        tv.register(OutcomeCell.self, forCellReuseIdentifier: OutcomeCell.id)
        tv.estimatedRowHeight = 200
        tv.dataSource = self
        tv.delegate = self
        tv.separatorStyle = .none
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func setupAppearance() {
        view.backgroundColor = Theme.current.secondBackgroundColor
        planTitleLabel.textColor = Theme.current.mainTextColor
        planValueLabel.textColor = Theme.current.mainTextColor
        periodPlanLabel.textColor = Theme.current.mainTextColor
        outcomesTable.backgroundColor = Theme.current.secondBackgroundColor
        outcomesTable.reloadData()
    }
    
    override func setupViews() {
        setupAppearance()
        
        let button = UIBarButtonItem(title: "Edit".localized(), style: .plain, target: self, action: #selector(changeTapped))
        navigationItem.rightBarButtonItem = button
        let backButton = UIBarButtonItem()
        backButton.title = "Back".localized()
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        view.addSubview(outcomesTable)
        outcomesTable.snp.makeConstraints { make in
            make.top.equalTo(view).offset(navBarHeight)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(-view.safeAreaInsets.bottom - Const.mainOffset)
        }
    }
    
    @objc private func changeTapped() {
        presenter?.onEditTapped()
    }
}


extension CategoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter?.categoryOutcomes.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OutcomeCell.id, for: indexPath)
        if let presenter = presenter, indexPath.row < presenter.categoryOutcomes.count {
            (cell as? OutcomeCell)?.outcome = presenter.categoryOutcomes[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        planView.addSubview(planIcon)
        planIcon.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(Const.largeOffset)
            make.centerX.equalToSuperview()
            make.size.equalTo(Const.tvItemEstimatedHeight)
        }
        
        planView.addSubview(planTitleLabel)
        planTitleLabel.snp.makeConstraints { make in
            make.top.equalTo(planIcon.snp.bottom).offset(Const.mainOffset)
            make.centerX.equalToSuperview()
        }
        
        planView.addSubview(planValueLabel)
        planValueLabel.snp.makeConstraints { make in
            make.top.equalTo(planTitleLabel.snp.bottom)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-Const.largeOffset)
        }
        return planView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = Theme.current.secondBackgroundColor
        
        view.addSubview(periodPlanLabel)
        periodPlanLabel.snp.makeConstraints { make in
            make.top.trailing.equalToSuperview().inset(Const.largeOffset)
        }
        
        
        let finalLabel = UILabel()
        finalLabel.textAlignment = .right
        finalLabel.font = Const.boldTextFont
        finalLabel.textColor = Theme.current.mainTextColor
        if let presenter = presenter {
            let finalAmount = presenter.categoryOutcomes.reduce(0) { $0 + $1.spent }
            finalLabel.text = "Final".localized() + ": " + finalAmount.stringValue
        }
        
        view.addSubview(finalLabel)
        finalLabel.snp.makeConstraints { make in
            make.top.equalTo(periodPlanLabel.snp.bottom).offset(Const.largeOffset )
            make.trailing.equalToSuperview().inset(Const.largeOffset)
        }
        
        let freeLabel = UILabel()
        freeLabel.textAlignment = .right
        if let presenter = presenter {
            let attText = NSMutableAttributedString()
            attText.append(NSAttributedString(string: "Free".localized() + ": ", attributes: [.font: Const.boldTextFont, .foregroundColor: Theme.current.mainTextColor]))
            attText.append(NSAttributedString(string: presenter.categoryStat.remains.stringValue, attributes: [.font: Const.boldTextFont, .foregroundColor: presenter.categoryStat.remains >= 0 ? Theme.current.mainTextColor : Theme.current.minusTextColor ]))
            freeLabel.attributedText = attText
        }
        
        view.addSubview(freeLabel)
        freeLabel.snp.makeConstraints { make in
            make.top.equalTo(finalLabel.snp.bottom)
            make.trailing.bottom.equalToSuperview().inset(Const.largeOffset)
        }
        
        return view
    }
}

extension CategoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.onOutcomeTapped(at: indexPath.row)
    }
}

extension CategoryViewController: ICategoryViewController {
    func setCategory(_ categoryStat: CategoryStatistics) {
        let category = categoryStat.category
        if Int(category.image_id) < categoryIcons.count {
            planIcon.image = categoryIcons[Int(category.image_id)].icon
            planIcon.tintColor = categoryIcons[Int(category.image_id)].color
            planView.backgroundColor = categoryIcons[Int(category.image_id)].color.withAlphaComponent(0.2)
        }
        title = category.name
        planValueLabel.text = category.mth_plan.stringValue
        
        if categoryStat.period.value != .month {
            periodPlanLabel.text = categoryStat.period.value.planTitle + ": " + categoryStat.planned.stringValue
        }
        outcomesTable.reloadData()
    }
    
    func setOutcomes() {
        outcomesTable.reloadData()
    }
}
