//
//  CategoryWireframe.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 16.08.2021.
//

import UIKit

protocol ICategoryWireframe: IBaseWireframe {
    func presentCategoryEditorView(category: Category?, delegate: ICategoryEditorOutput)
    func presentOutcomeEditorView(outcome: Outcome, delegate: IOutcomeEditorOutput)
    func closeModule()
}


final class CategoryWireframe: ICategoryWireframe {
    var view: IBaseViewController?
    
    init(view: IBaseViewController) {
        self.view = view
    }
    
    static func buildModule(category: Category, delegate: ICategoryOutput, period: Period, periodOffset: Int = 0, card: Card? = nil) -> UIViewController {
        let view: ICategoryViewController = CategoryViewController()
        let wireframe: ICategoryWireframe = CategoryWireframe(view: view)
        let presenter: ICategoryPresenter = CategoryPresenter(wireframe: wireframe, view: view, delegate: delegate, category: category, period: period, periodOffset: periodOffset, card: card)
        view.presenter = presenter
        return view
    }
    
    func presentCategoryEditorView(category: Category?, delegate: ICategoryEditorOutput) {
        let vc = CategoryEditorWireframe.buildModule(category: category, delegate: delegate) as UIViewController
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        view?.present(vc, animated: true, completion: nil)
    }
    
    func presentOutcomeEditorView(outcome: Outcome, delegate: IOutcomeEditorOutput) {
        let card: Card? = nil
        let category: Category? = nil
        guard let vc = OutcomeEditorWireframe.buildModule(card: card, category: category, outcome: outcome, delegate: delegate) else {
            print("Cannot resolve OutcomeEditor module!")
            return
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        view?.present(vc, animated: true, completion: nil)
    }
    
    func closeModule() {
        view?.navigationController?.popViewController(animated: true)
    }
}
