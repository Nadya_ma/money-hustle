//
//  OutcomeCell.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 17.08.2021.
//

import UIKit
import SnapKit


final class OutcomeCell: UITableViewCell {
    static let id = "OutcomeCell"
    
    private let dataLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = Const.descriptionFont
        return label
    }()
    private let detailsLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = Const.addDetailsFont
        return label
    }()
    private let amountLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = Const.boldTextFont
        return label
    }()
    private let dividerView: UIView = {
        let view = UIView()
        view.backgroundColor = Theme.current.separatorColor.withAlphaComponent(0.3)
        return view
    }()
    
    var outcome: Outcome? {
        didSet {
            if let outcome = outcome {
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: LocalizationManager.shared.locale)
                dateFormatter.dateFormat = "E, d MMM yy, HH:mm"
                dataLabel.text = dateFormatter.string(from: outcome.date)
                detailsLabel.text = outcome.comment ?? ""
                amountLabel.text = outcome.spent.stringValue
            }
        }
    }
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        setupAppearance()

        contentView.addSubview(amountLabel)
        amountLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().inset(Const.largeOffset)
        }
        
        contentView.addSubview(dataLabel)
        dataLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(Const.mainOffset)
            make.leading.equalToSuperview().offset(Const.largeOffset)
            make.trailing.equalTo(amountLabel.snp.leading)
        }
        
        contentView.addSubview(detailsLabel)
        detailsLabel.snp.makeConstraints { make in
            make.top.equalTo(dataLabel.snp.bottom).offset(Const.mainOffset)
            make.leading.equalToSuperview().offset(Const.largeOffset)
            make.trailing.equalTo(amountLabel.snp.leading)
        }
        
        contentView.addSubview(dividerView)
        dividerView.snp.makeConstraints { make in
            make.top.equalTo(detailsLabel.snp.bottom).offset(Const.mainOffset)
            make.leading.equalToSuperview().inset(Const.largeOffset)
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setupAppearance()
    }
    
    private func setupAppearance() {
        contentView.backgroundColor = Theme.current.mainBackgroundColor
        dataLabel.textColor = Theme.current.mainTextColor
        detailsLabel.textColor = Theme.current.mainTextColor.withAlphaComponent(0.7)
        amountLabel.textColor = Theme.current.mainTextColor
    }
}
