//
//  CategoryEditorPresenter.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 10.08.2021.
//

import Foundation

protocol ICategoryEditorOutput: AnyObject {
    func categoryEdited()
    func categoryDeleted()
}

protocol ICategoryEditorPresenter: IBasePresenter {
    func viewBackTapped()
    func saveTapped(name: String, value: String, iconId: Int)
    func deleteTapped()
}


final class CategoryEditorPresenter: ICategoryEditorPresenter {
    private let wireframe: ICategoryEditorWireframe
    private weak var view: ICategoryEditorViewController?
    private weak var delegate: ICategoryEditorOutput?
    
    private var category: Category?
    
    init(wireframe: ICategoryEditorWireframe, view: ICategoryEditorViewController?, delegate: ICategoryEditorOutput, category: Category?) {
        self.wireframe = wireframe
        self.view = view
        self.delegate = delegate
        self.category = category
    }
    
    
    func viewDidLoad() {
        if category != nil {
            view?.setCategory(category: category!)
        }
    }
    
    func viewBackTapped() {
        wireframe.closeModule()
    }
    
    func saveTapped(name: String, value: String, iconId: Int) {
        let monthPlan = Double(value) ?? 0
        if let category = category {
            category.configure(name: name, mthPlan: monthPlan, imageId: iconId)
        } else {
            let newCategory = coreDataManager.createObject(of: Category.self)
            newCategory.configure(name: name, mthPlan: monthPlan, imageId: iconId)
        }
        coreDataManager.saveChanges()
        delegate?.categoryEdited()
        wireframe.closeModule()
    }
    
    func deleteTapped() {
        wireframe.presentConfirmationAlert(question: "Are you sure you want to delete this?".localized(), okHandler: { [weak self] _ in
            guard  let category = self?.category else { return }
            category.isArchived = true
            self?.coreDataManager.saveChanges()
            self?.delegate?.categoryDeleted()
            self?.wireframe.closeModule()
        }, cancelHandler: nil)
    }
}
