//
//  CategoryEditorViewController.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 10.08.2021.
//

import UIKit
import UPCarouselFlowLayout

protocol ICategoryEditorViewController: IBaseViewController {
    var presenter: ICategoryEditorPresenter? { get set }
    
    func setCategory(category: Category)
}


final class CategoryEditorViewController: BaseViewController {
    var presenter: ICategoryEditorPresenter?
    
    private lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.alwaysBounceVertical = true
        view.alwaysBounceHorizontal = false
        view.backgroundColor = .clear
        view.showsVerticalScrollIndicator = false
        return view
    }()
    private lazy var mainContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    private let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Const.bigCornerRadius
        view.isUserInteractionEnabled = true
        return view
    }()
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "New category".localized()
        return label
    }()
    private let nameField: CommonTextField = {
        let field = CommonTextField()
        field.title = "Name".localized()
        return field
    }()
    private lazy var iconsCollectionView: UICollectionView = {
        let collectionLayout = UPCarouselFlowLayout()
        collectionLayout.scrollDirection = .horizontal
        collectionLayout.itemSize = Const.iconCvItemSize
        collectionLayout.spacingMode = .fixed(spacing: Const.largeOffset)
        collectionLayout.sideItemAlpha = 0.5
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionLayout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(CategoryCell.self, forCellWithReuseIdentifier: CategoryCell.id)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    private var iconId: Int?
    private let monthPlanField: CommonAmountTextField = {
        let field = CommonAmountTextField()
        field.title = "Month plan".localized()
        return field
    }()
    private let saveButton: UIButton = {
        var button = UIButton()
        button.setTitle("Save".localized(), for: .normal)
        button.layer.cornerRadius = Const.cornerRadius
        button.addTarget(self, action: #selector(saveTapped), for: .touchUpInside)
        return button
    }()
    private let deleteButton: UIButton = {
        var button = UIButton()
        button.setTitle("Delete".localized(), for: .normal)
        button.addTarget(self, action: #selector(deleteTapped), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subscribeToShowKeyboardNotifications()
        presenter?.viewDidLoad()
    }
    
    override func setupViews() {
        view.backgroundColor = .black.withAlphaComponent(0.3)
        setupAppearance()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:))))
        
        let containerWidth = min(view.frame.width, view.frame.height)
        
        view.addSubview(scrollView)
        scrollView.snp.remakeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        scrollView.addSubview(mainContainerView)
        mainContainerView.snp.remakeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalTo(view.snp.width)
        }

        mainContainerView.addSubview(containerView)
        containerView.snp.remakeConstraints { make in
            make.width.equalTo(containerWidth * 0.67)
            make.top.equalToSuperview().offset(120)
            make.centerX.equalToSuperview()
        }
        
        containerView.addSubview(titleLabel)
        titleLabel.snp.remakeConstraints { make in
            make.top.leading.trailing.equalToSuperview().inset(Const.largeOffset)
        }
        
        containerView.addSubview(nameField)
        nameField.snp.remakeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.equalToSuperview().inset(Const.largeOffset)
            make.height.equalTo(2*Const.iconCvItemSize.height)
        }
        
        containerView.addSubview(iconsCollectionView)
        iconsCollectionView.snp.remakeConstraints { make in
            make.top.equalTo(nameField.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.equalToSuperview().inset(Const.largeOffset)
            make.height.equalTo(Const.iconCvItemSize.height)
        }
        
        containerView.addSubview(monthPlanField)
        monthPlanField.snp.remakeConstraints { make in
            make.top.equalTo(iconsCollectionView.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.equalToSuperview().inset(Const.largeOffset)
            make.height.equalTo(2*Const.iconCvItemSize.height)
        }
        
        containerView.addSubview(saveButton)
        saveButton.snp.remakeConstraints { make in
            make.top.equalTo(monthPlanField.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.bottom.equalToSuperview().inset(Const.largeOffset)
            make.height.equalTo(Const.buttonHeight)
        }
        
        mainContainerView.addSubview(deleteButton)
        deleteButton.snp.remakeConstraints { make in
            make.top.equalTo(containerView.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.equalTo(containerView)
            make.height.equalTo(Const.buttonHeight)
            make.bottom.equalToSuperview()
        }
    }
    
    override func setupAppearance() {
        containerView.backgroundColor = Theme.current.mainBackgroundColor
        titleLabel.textColor = Theme.current.mainTextColor
        saveButton.backgroundColor = Theme.current.separatorColor
        deleteButton.setTitleColor(Theme.current.minusTextColor, for: .normal)
    }
    
    override func setupTransitionConstraints() {
        setupViews()
    }
    
    @objc func viewTapped(_ gr: UITapGestureRecognizer) {
        let point = gr.location(in: gr.view)
        if !containerView.frame.contains(point) {
            presenter?.viewBackTapped()
        }
    }
    
    @objc func saveTapped() {
        guard let name = nameField.value, let value = monthPlanField.value else { return }
        let iconId = iconId ?? 0
        presenter?.saveTapped(name: name, value: value, iconId: iconId)
    }
    
    @objc func deleteTapped() {
        presenter?.deleteTapped()
    }
}


extension CategoryEditorViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        categoryIcons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCell.id, for: indexPath)
        (cell as? CategoryCell)?.image = categoryIcons[indexPath.row].icon
        (cell as? CategoryCell)?.color = categoryIcons[indexPath.row].color
        return cell
    }
}

extension CategoryEditorViewController: UICollectionViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let point = view.convert(iconsCollectionView.center, to: iconsCollectionView)
        let max = Const.iconCvItemSize.width + Const.largeOffset/2
        iconId = Int((point.x) / max)
    }
}

extension CategoryEditorViewController: ICategoryEditorViewController {
    func setCategory(category: Category) {
        titleLabel.text = "Change category".localized()
        nameField.value = category.name
        iconsCollectionView(at: Int(category.image_id))
        iconId = Int(category.image_id)
        monthPlanField.value = String(category.mth_plan)
        deleteButton.isHidden = false
    }
    
    private func iconsCollectionView(at index: Int) {
        iconsCollectionView.reloadData()
        iconsCollectionView.layoutIfNeeded()
        let firstCellRect = iconsCollectionView.layoutAttributesForItem(at: IndexPath(row: 0, section: 0))?.frame
        var rect = iconsCollectionView.layoutAttributesForItem(at: IndexPath(row: index, section: 0))?.frame
        if let firstCellRect = firstCellRect {
            rect = rect?.offsetBy(dx: 2*firstCellRect.width, dy: 0)
        }
        iconsCollectionView.scrollRectToVisible(rect!, animated: false)
    }
}

extension CategoryEditorViewController {
    private func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 142 // 128 xs
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        scrollView.contentInset.bottom = 0
    }
}


fileprivate class CategoryCell: UICollectionViewCell {
    static let id = "CategoryCell"
    
    private let imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    var image: UIImage? {
        didSet {
            imageView.image = image
        }
    }
    var color: UIColor? {
        didSet {
            imageView.tintColor = color
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
