//
//  CategoryEditorWireframe.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 10.08.2021.
//

import UIKit

protocol ICategoryEditorWireframe: IBaseWireframe {
    func closeModule()
}

final class CategoryEditorWireframe: ICategoryEditorWireframe {
    weak var view: IBaseViewController?
    
    init(view: IBaseViewController) {
        self.view = view
    }
    
    static func buildModule(category: Category?, delegate: ICategoryEditorOutput) -> UIViewController {
        let view: ICategoryEditorViewController = CategoryEditorViewController()
        let wireframe: ICategoryEditorWireframe = CategoryEditorWireframe(view: view)
        let presenter: ICategoryEditorPresenter = CategoryEditorPresenter(wireframe: wireframe, view: view, delegate: delegate, category: category)
        view.presenter = presenter
        return view
    }
    
    func closeModule() {
        view?.dismiss(animated: true, completion: nil)
    }
}
