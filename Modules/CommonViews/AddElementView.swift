//
//  AddElementView.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 04.08.2021.
//

import UIKit

final class AddElementView: UIView {
    
    private let plusView: UIImageView = {
        let view = UIImageView()
        view.tintColor = Theme.current.separatorColor.withAlphaComponent(0.3)
        view.contentMode = .scaleAspectFill
        view.image = UIImage(systemName: "plus.circle.fill")
        return view
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupViews()
    }
    
    private func setupViews() {
        let diameter = min(frame.width, frame.height) / 2
        
        addSubview(plusView)
        plusView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(diameter)
        }
    }
}
