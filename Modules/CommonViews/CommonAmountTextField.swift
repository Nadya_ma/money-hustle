//
//  CommonAmountTextField.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 02.11.2021.
//

import UIKit

final class CommonAmountTextField: CommonTextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        super.keyBoardType = .decimalPad
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var value: String? {
        get {
            super.value?.replacingOccurrences(of: " ", with: "")
        }
        set {
            if let parts = newValue?.components(separatedBy: ".") {
                var formattedString = parts.first?.formatAmountString()
                
                if parts.count > 1 {
                    let floating = parts[1].prefix(2)
                    if floating != "0" && floating != "00" {
                        formattedString?.append(".")
                        formattedString?.append(contentsOf: floating)
                    }
                }
                
                super.value = formattedString
            }
        }
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = NSString(string: textField.text ?? "").replacingCharacters(in: range, with: string).replacingOccurrences(of: ",", with: ".").replacingOccurrences(of: " ", with: "")
        if text.isEmpty { return true }
        // Return false if zero is leading
        if textField.text?.isEmpty == true && string == "0" { return false }
        if Double(text) == nil { return false }
        
        let parts = text.components(separatedBy: ".")
        var formattedString = parts.first?.formatAmountString()
        
        if parts.count > 1 {
            let floatingString = parts[1].prefix(2)
            formattedString?.append(".")
            formattedString?.append(contentsOf: floatingString)
        }
        
        textField.text = formattedString
        textField.invalidateIntrinsicContentSize()
        
        return false
    }
}
