//
//  CommonNavigationBar.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 02.11.2021.
//

import UIKit

final class CommonNavigationController: UINavigationController {
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        switch traitCollection.userInterfaceStyle {
        case .light, .unspecified: Theme.current = .light
        case .dark: Theme.current = .dark
        @unknown default: return
        }
        self.navigationBar.barTintColor = Theme.current.mainBackgroundColor
        self.navigationBar.titleTextAttributes = [.foregroundColor: Theme.current.mainTextColor]
        self.view.tintColor = Theme.current.separatorColor
    }
}
