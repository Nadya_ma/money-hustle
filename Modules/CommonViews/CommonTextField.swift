//
//  CommonTextView.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 10.08.2021.
//

import UIKit

class CommonTextField: UIView {
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = Theme.current.mainTextColor
        label.textAlignment = .center
        return label
    }()
    private lazy var field: UITextField = {
        let field = UITextField()
        field.layer.cornerRadius = Const.cornerRadius
        field.layer.borderColor = Theme.current.separatorColor.cgColor
        field.layer.borderWidth = Const.borderWidth
        field.textAlignment = .center
        field.textColor = Theme.current.mainTextColor
        field.delegate = self
        return field
    }()
    
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    var value: String? {
        get {
            isSafe ? _cachedValue : field.text
        }
        set {
            field.text = newValue
            _cachedValue = newValue ?? ""
        }
    }
    private var _cachedValue = ""
    var isEditable: Bool = true {
        didSet {
            field.isEnabled = false
        }
    }
    var keyBoardType: UIKeyboardType = .default {
        didSet {
            field.keyboardType = keyBoardType
        }
    }
    var isSafe: Bool = false
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupViews()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        titleLabel.textColor = Theme.current.mainTextColor
        field.textColor = Theme.current.mainTextColor
    }
    
    private func setupViews() {
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(22)
        }
        
        addSubview(field)
        field.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(Const.mainOffset)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
}


extension CommonTextField: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if isSafe {
            var previousText = textField.text ?? ""
            if !string.isEmpty {
                _cachedValue.append(string)
                previousText.append("*")
            } else {
                previousText = String(previousText.dropLast())
                _cachedValue = String(_cachedValue.dropLast())
            }
            
            textField.text = previousText
            print (_cachedValue)
            return false
        } else {
            return true
        }
    }
}
