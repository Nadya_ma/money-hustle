//
//  PeriodCell.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 15.09.2021.
//

import UIKit

final class PeriodCell: UIView {
    static let id = "PeriodCell"
    
    private let lineView = UIView()
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    
    var period: Period.Value? {
        didSet {
            guard let period = period else { return }
            nameLabel.text = period.title
        }
    }
    var isChoosen = false {
        didSet {
            nameLabel.font = isChoosen ? Const.h4BoldTitleFont : Const.h4TitleFont
            lineView.backgroundColor = Theme.current.separatorColor.withAlphaComponent(isChoosen ? 0.7 : 0.2)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addSubview(nameLabel)
        nameLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.width.equalTo(70)
            make.centerX.equalToSuperview()
        }
        
        addSubview(lineView)
        lineView.snp.makeConstraints { make in
            make.top.equalTo(nameLabel.snp.bottom).offset(Const.mainOffset/2)
            make.leading.trailing.equalTo(nameLabel)
            make.height.equalTo(4)
            make.bottom.equalToSuperview()
        }
    }
}
