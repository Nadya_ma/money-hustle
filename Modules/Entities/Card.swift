//
//  Card.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 29.07.2021.
//
//

import Foundation
import CoreData

@objc(Card)
public class Card: NSManagedObject {
    
    @NSManaged public var id: Int16
    @NSManaged public var name: String
    @NSManaged public var amount: Double
    @NSManaged public var isArchived: Bool
    
    public override var description: String {
        "id: \(self.id), name: \(self.name), amount: \(self.amount), isArchived: \(self.isArchived)"
    }
    
    func configure(name: String, amount: Double) {
        if id == 0 {
            self.id = nextId()
        }
        self.name = name
        self.amount = amount
        self.isArchived = false
    }
    
    @nonobjc public static func fetchRequest() -> NSFetchRequest<Card> {
        return NSFetchRequest<Card>(entityName: "Card")
    }
    
    private func nextId() -> Int16 {
        let defaults = UserDefaults.standard
        let value = defaults.integer(forKey: "CardNextId") // if not key, returns 0
        defaults.setValue(value+1, forKey: "CardNextId")
        return Int16(value+1)
    }
}
