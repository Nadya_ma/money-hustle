//
//  Category.swift
//  
//
//  Created by Nadya Mamysheva on 02.08.2021.
//
//

import Foundation
import CoreData

@objc(Category)
public class Category: NSManagedObject {
    
    @NSManaged public var id: Int16
    @NSManaged public var name: String
    @NSManaged public var mth_plan: Double
    @NSManaged public var image_id: Int16
    @NSManaged public var isArchived: Bool
    @NSManaged public var position: Int16
    
    public override var description: String {
        "id: \(self.id), name: \(self.name), mth_plan: \(self.mth_plan), image_id: \(self.image_id), isArchived: \(self.isArchived), position: \(self.position)"
    }
    
    func configure(name: String, mthPlan: Double, imageId: Int) {
        if id == 0 {
            id = nextId()
            position = id
        }
        self.name = name
        mth_plan = mthPlan
        image_id = Int16(imageId)
        isArchived = false
    }
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Category> {
        return NSFetchRequest<Category>(entityName: "Category")
    }
    
    private func nextId() -> Int16 {
        let defaults = UserDefaults.standard
        let value = defaults.integer(forKey: "CategoryNextId") // if not key, returns 0
        defaults.setValue(value+1, forKey: "CategoryNextId")
        return Int16(value+1)
    }
}
