//
//  Outcome.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 06.08.2021.
//

import Foundation
import CoreData

@objc(Outcome)
public class Outcome: NSManagedObject {
    
    @NSManaged public var id: Int16
    @NSManaged public var date: Date
    @NSManaged public var card_id: Int16
    @NSManaged public var category_id: Int16
    @NSManaged public var spent: Double
    @NSManaged public var comment: String?
    
    public override var description: String {
        "date: \(String(describing: date.description)), card_id: \(card_id), category_id: \(category_id), spent: \(spent), comment: \(comment ?? "")"
    }
    
    func configure(card: Card, category: Category, spent: Double, comment: String? = nil, date: Date) {
        if id == 0 {
            id = nextId()
        }
        self.date = date
        card_id = card.id
        category_id = category.id
        self.spent = spent
        self.comment = comment
    }
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Outcome> {
        return NSFetchRequest<Outcome>(entityName: "Outcome")
    }
    
    private func nextId() -> Int16 {
        let defaults = UserDefaults.standard
        let value = defaults.integer(forKey: "OutcomeNextId") // if not key, returns 0
        defaults.setValue(value+1, forKey: "OutcomeNextId")
        return Int16(value+1)
    }
}
