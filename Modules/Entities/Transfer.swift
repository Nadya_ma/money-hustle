//
//  Transfer.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 24.08.2021.
//

import Foundation
import CoreData

@objc(Transfer)
public class Transfer: NSManagedObject {
    
    @NSManaged public var id: Int16
    @NSManaged public var date: Date
    @NSManaged public var from_card_id: Int16
    @NSManaged public var to_card_id: Int16
    @NSManaged public var amount: Double
    
    public override var description: String {
        "date: \(String(describing: date.description)), from_card_id: \(from_card_id), to_card_id: \(to_card_id), amount: \(amount)"
    }
    
    func configure(fromCard: Card, toCard: Card, amount: Double, date: Date) {
        if id == 0 {
            id = nextId()
        }
        self.date = date
        from_card_id = fromCard.id
        to_card_id = toCard.id
        self.amount = amount
    }
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Outcome> {
        return NSFetchRequest<Outcome>(entityName: "Transfer")
    }
    
    private func nextId() -> Int16 {
        let defaults = UserDefaults.standard
        let value = defaults.integer(forKey: "TransferNextId") // if not key, returns 0
        defaults.setValue(value+1, forKey: "TransferNextId")
        return Int16(value+1)
    }
}
