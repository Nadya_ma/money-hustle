//
//  Date+extension.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 31.08.2021.
//

import Foundation

extension Date {

    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var startOfWeek: Date {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.calendar, .yearForWeekOfYear, .weekOfYear], from: self)
        return calendar.date(from: components)!
    }
    
    var startOfMonth: Date {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month], from: self)
        return  calendar.date(from: components)!
    }
    
    var startOfWeekOrMonth: Date {
        let weekStart = self.startOfWeek
        let monthStart = self.startOfMonth
        let start = weekStart < monthStart ? monthStart : weekStart
        return start
    }
    
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
    
    var endOfWeek: Date {
        var components = DateComponents()
        components.weekOfYear = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfWeek)!
    }
    
    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfMonth)!
    }
    
    var endOfWeekOrMonth: Date {
        let weekEnd = self.endOfWeek
        let monthEnd = self.endOfMonth
        let end = monthEnd < weekEnd ? monthEnd : weekEnd
        return end
    }
}
