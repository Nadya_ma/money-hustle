//
//  Double+extension.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 02.08.2021.
//

import Foundation

extension Double {
    var stringValue: String {
        var string = String(String(self).split(separator: ".")[0])
        
        if string.count > 3 {
            string.insert(" ", at: string.getIndexFromEnd(at: 3))
        }
        
        if string.count > 7 {
            string.insert(" ", at: string.getIndexFromEnd(at: 7))
        }
        
        if string.count > 11 {
            string.insert(" ", at: string.getIndexFromEnd(at: 11))
        }
        
        if String(self).split(separator: ".").count > 1 {
            let floating = String(String(self).split(separator: ".")[1].prefix(2))
            if floating != "0" && floating != "00" {
                string.append(".")
                string.append(floating)
            }
        }
        
        return string
    }
}
