//
//  String+extension.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 02.08.2021.
//

import Foundation

extension String {
    public func getIndexFromEnd(at: Int) -> String.Index {
        return index(endIndex, offsetBy: -at)
    }
    
    public func formatAmountString() -> String {
        var tempString = self
        
        if tempString.count > 3 {
            tempString.insert(Character(" "), at: tempString.getIndexFromEnd(at: 3))
        }
        if tempString.count > 7 {
            tempString.insert(Character(" "), at: tempString.getIndexFromEnd(at: 7))
        }
        if tempString.count > 11 {
            tempString.insert(Character(" "), at: tempString.getIndexFromEnd(at: 11))
        }
        
        if tempString.count > 15 {
            tempString.insert(Character(" "), at: tempString.getIndexFromEnd(at: 15))
        }
        
        return tempString
    }
    
    public func localized() -> String {
        NSLocalizedString(self, tableName: "Localizable (\(LocalizationManager.shared.locale))", bundle: Bundle.main, comment: "")
    }
}
