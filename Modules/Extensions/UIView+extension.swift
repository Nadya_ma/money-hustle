//
//  UIView+extension.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 02.08.2021.
//

import UIKit

extension UIView {
    
    func setFadingToEdgesBackground(_ color: UIColor, to fadeColor: UIColor) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [color.cgColor, fadeColor.cgColor]
        gradient.type = .radial
        gradient.startPoint = CGPoint(x: 0.5, y: 0.5)
        gradient.endPoint = CGPoint(x: 0.0, y: 0.0)
        self.layer.insertSublayer(gradient, at: 0)
    }
}
