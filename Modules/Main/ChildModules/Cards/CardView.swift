//
//  CardView.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 30.07.2021.
//

import UIKit
import SnapKit

final class CardView: UIView {
    private lazy var containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Const.cornerRadius
        return view
    }()
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = Const.descriptionFont
        label.textColor = Theme.current.mainTextColor
        return label
    }()
    private lazy var amountLabel: UILabel = {
        let label = UILabel()
        label.font = Const.descriptionFont
        label.textColor = Theme.current.mainTextColor
        label.textAlignment = .center
        return label
    }()
    
    var card: Card? {
        didSet {
            guard let card = card else { return }
            nameLabel.text = card.name.uppercased()
            amountLabel.text = card.amount.stringValue
            let colorIndex: Int = Int(card.id) % cardColors.count
            containerView.backgroundColor = cardColors[colorIndex]
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        backgroundColor = .clear
        
        addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview().inset(Const.mainOffset)
        }
        addSubview(nameLabel)
        nameLabel.snp.makeConstraints { make in
            make.center.equalTo(containerView)
        }
        addSubview(amountLabel)
        amountLabel.snp.makeConstraints { make in
            make.top.equalTo(containerView.snp.bottom).offset(Const.mainOffset)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func prepareForReuse() {
        nameLabel.text = ""
        nameLabel.textColor = Theme.current.mainTextColor
        amountLabel.text = ""
        amountLabel.textColor = Theme.current.mainTextColor
    }
}
