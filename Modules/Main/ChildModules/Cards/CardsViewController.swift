//
//  CardsViewController.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 04.08.2021.
//

import UIKit

protocol ICardsViewController: IBaseViewController {
    var presenter: IMainPresenter? { get set }
    
    func reloadData()
}


final class CardsViewController: BaseViewController {
    
    var presenter: IMainPresenter?
    
    private lazy var cardsCollectionView: UICollectionView = {
        let collectionLayout = UICollectionViewFlowLayout()
        collectionLayout.scrollDirection = .horizontal
        collectionLayout.itemSize = Const.cardsCvItemSize
        collectionLayout.minimumInteritemSpacing = Const.cvSpacing
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionLayout)
        collectionView.register(CardCell.self, forCellWithReuseIdentifier: CardCell.id)
        collectionView.register(CardFooter.self, forCellWithReuseIdentifier: CardFooter.id)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.dragInteractionEnabled = true
        collectionView.dragDelegate = self
        collectionView.dropDelegate = self
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    override func setupViews() {
        view.backgroundColor = Theme.current.secondBackgroundColor
        
        view.addSubview(cardsCollectionView)
        cardsCollectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    override func setupAppearance() {
        view.backgroundColor = Theme.current.secondBackgroundColor
        cardsCollectionView.reloadData()
    }
}


extension CardsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        (presenter?.cardList.count ?? 0) + 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cardList: [Card] = presenter?.cardList ?? []
        
        if cardList.count == indexPath.row { // last element
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CardFooter.id, for: indexPath)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CardCell.id, for: indexPath)
            (cell as? CardCell)?.configure(card: cardList[indexPath.row])
            return cell
        }
    }
}

extension CardsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index: Int? = presenter?.cardList.count == indexPath.row ? nil : indexPath.row
        presenter?.cardTapped(at: index)
    }
}

extension CardsViewController: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        if let card = presenter?.cardList[indexPath.row] {
            let provider = NSItemProvider(object: card.name as NSString)
            let item = UIDragItem(itemProvider: provider)
            item.localObject = card
            return [item]
        }
        return []
    }
}

extension CardsViewController: UICollectionViewDropDelegate {
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        return UICollectionViewDropProposal(operation: .move, intent: .insertIntoDestinationIndexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        if let cardIndex = coordinator.destinationIndexPath?.row,
           let card = coordinator.items.first?.dragItem.localObject as? Card {
            presenter?.transfer(to: cardIndex, from: card)
        }
    }
}

extension CardsViewController: ICardsViewController {
    func reloadData() {
        cardsCollectionView.reloadData()
    }
}


fileprivate class CardCell: UICollectionViewCell {
    static let id = "CardCell"
    private let cardView = CardView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        layer.cornerRadius = Const.bigCornerRadius
        
        addSubview(cardView)
        cardView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func configure(card: Card?) {
        cardView.card = card
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cardView.prepareForReuse()
    }
}


fileprivate class CardFooter: UICollectionViewCell {
    static let id = "CardFooter"
    
    private let addElementView = AddElementView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addSubview(addElementView)
        addElementView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
