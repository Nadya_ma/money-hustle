//
//  CategoriesViewController.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 02.08.2021.
//

import UIKit

protocol ICategoriesViewController: IBaseViewController {
    var presenter: IMainPresenter? { get set }
    
    func reloadData()
}


final class CategoriesViewController: BaseViewController {
    
    var presenter: IMainPresenter?
    
    private lazy var categoriesCollectionView: UICollectionView = {
        let collectionLayout = UICollectionViewFlowLayout()
        collectionLayout.scrollDirection = .vertical
        collectionLayout.itemSize = Const.categoryCvItemSize
        collectionLayout.minimumInteritemSpacing = Const.cvSpacing
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionLayout)
        collectionView.register(MainCategoryCell.self, forCellWithReuseIdentifier: MainCategoryCell.id)
        collectionView.register(CategoryFooter.self, forCellWithReuseIdentifier: CategoryFooter.id)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.dragInteractionEnabled = true
        collectionView.dragDelegate = self
        collectionView.dropDelegate = self
        collectionView.backgroundColor = Theme.current.mainBackgroundColor
        return collectionView
    }()
    
    override func setupViews() {
        view.addSubview(categoriesCollectionView)
        categoriesCollectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    override func setupAppearance() {
        categoriesCollectionView.backgroundColor = Theme.current.mainBackgroundColor
        categoriesCollectionView.reloadData()
    }
}

extension CategoriesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        (presenter?.categoryList.count ?? 0) + 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let categoryList: [CategoryStatistics] = presenter?.categoryList ?? []
        
        if categoryList.count == indexPath.row { // last element
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryFooter.id, for: indexPath)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MainCategoryCell.id, for: indexPath)
            (cell as? MainCategoryCell)?.configure(category: categoryList[indexPath.row])
            return cell
        }
    }
}

extension CategoriesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index: Int? = presenter?.categoryList.count == indexPath.row ? nil : indexPath.row
        presenter?.categoryTapped(at: index)
    }
}

extension CategoriesViewController: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        if let category = presenter?.categoryList[indexPath.row] {
            let provider = NSItemProvider(object: category.name as NSString)
            let item = UIDragItem(itemProvider: provider)
            item.localObject = category
            return [item]
        }
        return []
    }
}

extension CategoriesViewController: UICollectionViewDropDelegate {
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        if let id = coordinator.destinationIndexPath?.row {
            if let card = coordinator.items.first?.dragItem.localObject as? Card {
                presenter?.addOutcome(at: id, for: card)
            } else if let category = coordinator.items.first?.dragItem.localObject as? CategoryStatistics {
                presenter?.moveCategoryToIndex(category.category, to: id)
                if let sourcePath = coordinator.items.first?.sourceIndexPath,
                   let destPath = coordinator.destinationIndexPath {
                    collectionView.moveItem(at: sourcePath, to: destPath)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        if let _ = session.items.first?.localObject as? Card {
            return UICollectionViewDropProposal(operation: .move, intent: .insertIntoDestinationIndexPath)
        } else {
            return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
        }
    }
}

extension CategoriesViewController: ICategoriesViewController {
    func reloadData() {
        categoriesCollectionView.reloadData()
    }
}


fileprivate class MainCategoryCell: UICollectionViewCell {
    static let id = "MainCategoryCell"
    private let categoryView = CategoryView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addSubview(categoryView)
        categoryView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(16)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func configure(category: CategoryStatistics?) {
        categoryView.category = category
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        categoryView.prepareForReuse()
    }
}

fileprivate class CategoryFooter: UICollectionViewCell {
    static let id = "CategoryFooter"
    
    private let addElementView = AddElementView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addElementView.isUserInteractionEnabled = true
        addSubview(addElementView)
        addElementView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
