//
//  CategoryView.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 02.08.2021.
//

import UIKit
import SnapKit

final class CategoryView: UIView {
    private let imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = Const.boldDescriptionFont
        label.textColor = Theme.current.mainTextColor
        label.textAlignment = .center
        return label
    }()
    private lazy var plannedLabel: UILabel = {
        let label = UILabel()
        label.font = Const.addDetailsFont
        label.textColor = Theme.current.mainTextColor
        label.textAlignment = .center
        return label
    }()
    private lazy var spentLabel: UILabel = {
        let label = UILabel()
        label.font = Const.addDetailsFont
        label.textColor = Theme.current.mainTextColor
        label.textAlignment = .center
        return label
    }()
    private lazy var remainsLabel: UILabel = {
        let label = UILabel()
        label.font = Const.boldAddDetailsFont
        label.textColor = Theme.current.mainTextColor
        label.textAlignment = .right
        return label
    }()
    
    var category: CategoryStatistics? {
        didSet {
            guard let category = category else { return }
            nameLabel.text = category.name
            imageView.tintColor = category.color
            imageView.image = category.image
            plannedLabel.text = category.planned.stringValue
            spentLabel.text = (category.spent > 0 ? "-" : "") + category.spent.stringValue
            remainsLabel.textColor = category.remains < 0 ? Theme.current.minusTextColor : Theme.current.mainTextColor
            remainsLabel.text = category.remains.stringValue
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addSubview(nameLabel)
        nameLabel.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(Const.addDetailsLabelHeight)
        }
        
        addSubview(remainsLabel)
        remainsLabel.snp.makeConstraints { make in
            make.bottom.centerX.equalToSuperview()
            make.height.equalTo(Const.addDetailsLabelHeight)
            make.width.greaterThanOrEqualTo(32)
        }
        
        addSubview(spentLabel)
        spentLabel.snp.makeConstraints { make in
            make.trailing.equalTo(remainsLabel)
            make.bottom.equalTo(remainsLabel.snp.top)
            make.height.equalTo(Const.addDetailsLabelHeight)
        }
        
        addSubview(plannedLabel)
        plannedLabel.snp.makeConstraints { make in
            make.trailing.equalTo(remainsLabel)
            make.bottom.equalTo(spentLabel.snp.top)
            make.height.equalTo(Const.addDetailsLabelHeight)
        }
        
        addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.top.equalTo(nameLabel.snp.bottom).offset(Const.mainOffset)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(plannedLabel.snp.top).offset(-Const.mainOffset)
        }
    }
    
    func prepareForReuse() {
        nameLabel.text = ""
        nameLabel.textColor = Theme.current.mainTextColor
        plannedLabel.text = ""
        plannedLabel.textColor = Theme.current.mainTextColor
        imageView.image = nil
        spentLabel.textColor = Theme.current.mainTextColor
    }
}
