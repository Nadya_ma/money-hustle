//
//  HeaderView.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 04.08.2021.
//

import UIKit

protocol IHeaderView: UIView {
    func setPeriod(_ period: Period)
    func setStatistics(_ statistics: PeriodStatistics)
    
    var periodTapped: ((Period.Value?) -> Void)? { get set }
}


final class HeaderView: UIView {
    
    var periodTapped: ((Period.Value?) -> Void)?
    
    private let periodsStack: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Const.stackSpacing
        stack.distribution = .fillEqually
        stack.axis = .horizontal
        return stack
    }()
    private let statisticsStack: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Const.stackSpacing
        stack.distribution = .fillEqually
        stack.axis = .horizontal
        return stack
    }()
    
    private var currentPeriod: Period?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupViews() {
        backgroundColor = Theme.current.secondBackgroundColor
        
        addSubview(periodsStack)
        periodsStack.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview().inset(Const.largeOffset)
        }
        
        addSubview(statisticsStack)
        statisticsStack.snp.makeConstraints { make in
            make.top.equalTo(periodsStack.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        backgroundColor = Theme.current.secondBackgroundColor
    }
}

extension HeaderView: IHeaderView {
    
    func setPeriod(_ period: Period) {
        currentPeriod = period
        let allCases = Period.Value.allCases
        let index = allCases.firstIndex(of: period.value) ?? 0
        
        periodsStack.arrangedSubviews.forEach { $0.removeFromSuperview() }
        allCases.enumerated().forEach { (id, value) in
            let view = PeriodCell()
            view.tag = id
            view.period = value
            view.isChoosen = id == index
            view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(periodTapped(_:))))
            periodsStack.addArrangedSubview(view)
        }
    }
    
    @objc private func periodTapped(_ gr: UITapGestureRecognizer) {
        if let index = gr.view?.tag,
           let choosenPeriodValue = Period.Value.init(intValue: index),
           currentPeriod?.value != choosenPeriodValue {
            periodTapped?(choosenPeriodValue)
        }
    }
    
    func setStatistics(_ statistics: PeriodStatistics) {
        statisticsStack.arrangedSubviews.forEach { $0.removeFromSuperview() }
        
        let barView = StatisticsBarView()
        barView.configure(spent: statistics.spent, remainActive: (statistics.remains + statistics.free), remainPlan: statistics.remains)
        statisticsStack.addArrangedSubview(barView)
        
        let listView = StatisticsListView()
        listView.configure(spent: statistics.spent, remainActive: (statistics.remains + statistics.free), remainPlan: statistics.remains)
        statisticsStack.addArrangedSubview(listView)
    }
}
