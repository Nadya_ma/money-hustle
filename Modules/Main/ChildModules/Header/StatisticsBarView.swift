//
//  StatisticsBarView.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 16.09.2021.
//

import KDCircularProgress

final class StatisticsBarView: UIView {
    private lazy var activesBar: KDCircularProgress = {
        let bar = KDCircularProgress()
        bar.startAngle = -90
        bar.clockwise = false
        bar.roundedCorners = true
        bar.glowMode = .noGlow
        bar.progressThickness = 0.5
        bar.trackThickness = 0.6
        return bar
    }()
    private lazy var spentsBar: KDCircularProgress = {
        let bar = KDCircularProgress()
        bar.startAngle = -90
        bar.clockwise = false
        bar.roundedCorners = true
        bar.glowMode = .noGlow
        bar.progressThickness = 0.4
        bar.trackThickness = 0.5
        return bar
    }()
    private lazy var freeLabel: UILabel = {
        let label = UILabel()
        label.font = Const.boldDescriptionFont
        label.textColor = Theme.current.mainTextColor
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        freeLabel.textColor = Theme.current.mainTextColor
    }
    
    private func setupViews() {
        addSubview(activesBar)
        activesBar.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(CGSize(width: 92, height: 92))
        }
        
        addSubview(spentsBar)
        spentsBar.snp.makeConstraints { make in
            make.top.centerX.bottom.equalToSuperview()
            make.size.equalTo(CGSize(width: 124, height: 124))
        }
        
        addSubview(freeLabel)
        freeLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
    
    func configure(spent: Double, remainActive: Double, remainPlan: Double) {
        let anglePlan = remainPlan > 0 ? 360 * (remainPlan / (spent + remainPlan)) : 0
        activesBar.set(colors: Theme.current.separatorColor)
        activesBar.trackColor = Theme.current.separatorColor.withAlphaComponent(0.3)
        
        let angleActive = remainActive > 0 ? 360 * (remainActive / (spent + remainActive)) : 0
        spentsBar.set(colors: Theme.current.secondSeparatorColor)
        spentsBar.trackColor = Theme.current.secondSeparatorColor.withAlphaComponent(0.3)
        
        activesBar.animate(fromAngle: 0, toAngle: angleActive, duration: 1.0, completion: nil)
        spentsBar.animate(fromAngle: 0, toAngle: anglePlan, duration: 1.0, completion: nil)
        
        let free = remainActive - remainPlan
        let smile = free > 0 ? "🙂" : "😨"
        freeLabel.text = free.stringValue + "\n" + smile
    }
}
