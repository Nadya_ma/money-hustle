//
//  StatisticsListView.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 20.09.2021.
//

import UIKit

final class StatisticsListView: UIView {
    private let stack: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Const.stackSpacing
        stack.axis = .vertical
        return stack
    }()
    
    private func makeStatistics(color: UIColor, title: String) -> UIView {
        let view = UIView()
        
        let circleView = UIView()
        circleView.layer.cornerRadius = 5
        circleView.backgroundColor = color
        view.addSubview(circleView)
        circleView.snp.makeConstraints { make in
            make.top.leading.bottom.equalToSuperview()
            make.size.equalTo(10)
        }
        
        let label = UILabel()
        label.font = Const.addDetailsFont
        label.textColor = Theme.current.mainTextColor
        label.textAlignment = .left
        label.text = title
        view.addSubview(label)
        label.snp.makeConstraints { make in
            make.leading.equalTo(circleView.snp.trailing).offset(Const.mainOffset)
            make.centerY.equalTo(circleView)
            make.trailing.equalToSuperview()
        }
        
        return view
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if let (spent, remainActive, remainPlan) = values {
            configure(spent: spent, remainActive: remainActive, remainPlan: remainPlan)
        }
    }
    
    private func setupViews() {
        addSubview(stack)
        stack.snp.makeConstraints { $0.centerY.equalToSuperview() }
    }
    
    private var values: (spent: Double, remainActive: Double, remainPlan: Double)?
    
    func configure(spent: Double, remainActive: Double, remainPlan: Double) {
        values = (spent, remainActive, remainPlan)
        stack.arrangedSubviews.forEach { $0.removeFromSuperview() }
        let total = makeStatistics(color: Theme.current.separatorColor.withAlphaComponent(0.3), title: (spent + remainActive).stringValue + " // " + "all actives".localized())
        stack.addArrangedSubview(total)
        let free = makeStatistics(color: Theme.current.separatorColor, title: remainActive.stringValue + " // " + "remaining actives".localized())
        stack.addArrangedSubview(free)
        let allPlans = makeStatistics(color: Theme.current.secondSeparatorColor.withAlphaComponent(0.3), title: (spent + remainPlan).stringValue + " // " + "all plans".localized())
        stack.addArrangedSubview(allPlans)
        let plans = makeStatistics(color: Theme.current.secondSeparatorColor, title: remainPlan.stringValue + " // " + "remaining plans".localized())
        stack.addArrangedSubview(plans)
    }
}
