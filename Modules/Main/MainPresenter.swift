//
//  MainPresenter.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 29.07.2021.
//

import Foundation
import CoreData

protocol IMainPresenter: IBasePresenter {
    var cardList: [Card] { get }
    var categoryList: [CategoryStatistics] { get }
    var isLockSet: Bool { get }
    
    func periodTapped(periodValue: Period.Value)
    func categoryTapped(at index: Int?)
    func cardTapped(at index: Int?)
    func addOutcome(at index: Int, for card: Card)
    func transfer(to index: Int?, from card: Card)
    func setCodeTapped()
    func moveCategoryToIndex(_ category: Category, to index: Int)
    func hideInactiveCategories(isHide: Bool)
    func statisticsTapped()
}


final class MainPresenter {
    
    private weak var view: IMainViewController?
    private var wireframe: IMainWireframe
    
    private var period: Period {
        get {
            let defaults = UserDefaults.standard
            let value = defaults.string(forKey: "Period")
            if let value = value, let periodValue = Period.Value(rawValue: value) {
                return Period(value: periodValue, isChoosen: true)
            } else {
                defaults.set(Period.Value.month.rawValue, forKey: "Period")
                return Period(value: .month, isChoosen: true)
            }
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue.value.rawValue, forKey: "Period")
        }
    }
    var cardList: [Card] {
        coreDataManager.getObjects(of: Card.self)
    }
    var categoryList: [CategoryStatistics] {
        var allCategories = calculateCategoryStatistics(period: period)
        if hideInactiveCategories {
            allCategories = allCategories.filter { $0.planned != 0 || $0.spent != 0 }
        }
        return allCategories.sorted(by: { $0.category.position < $1.category.position })
    }
    var isLockSet: Bool {
        sessionManager.isCodeSet
    }
    private var outcomes: [Outcome] {
        coreDataManager.getOutcomes(for: period)
    }
    private var mthOutcomes: [Outcome] {
        coreDataManager.getOutcomes(for: Period.init(value: .month, isChoosen: false))
    }
    private var totalOutcome = 0
    private var hideInactiveCategories: Bool {
        get {
            let defaults = UserDefaults.standard
            return defaults.bool(forKey: "HideInactiveCategories")
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: "HideInactiveCategories")
        }
    }
    
    
    init(wireframe: IMainWireframe, view: IMainViewController) {
        self.wireframe = wireframe
        self.view = view
    }
    
    deinit {
        coreDataManager.saveChanges()
    }
    
    func viewDidLoad() {
        view?.setPeriod(period)
        view?.reloadCards()
        view?.setSwitcherOn(hideInactiveCategories)
        view?.setStatistics(calculatePeriodStatistics(period: period))
        view?.reloadCategories()
        
        checkSession()
    }
    
    
    private func checkSession() {
        if sessionManager.isCodeSet {
            if !sessionManager.isAuth {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                    self?.wireframe.presentAuthView(delegate: self)
                }
            }
        } else {
            sessionManager.safeEntrance()
        }
    }
    
    private func calculatePeriodStatistics(period: Period) -> PeriodStatistics {
        
        let planned = categoryList.reduce(0.0) { (i, category) in i + category.planned }
        let remainsOnCard = cardList.reduce(0) { (i, card) in i + card.amount }
        let remainsToSpent = categoryList.reduce(0.0) { (i, category) in
            if category.planned - category.spent > 0 {
                return i + category.planned - category.spent
            } else {
                return i + 0
            }
        }
        let spent = outcomes.reduce(0) { (i, outcome) in i + outcome.spent }
        let freeAbs = (remainsOnCard + spent) * period.weight - spent - remainsToSpent
        
        return PeriodStatistics(planned: planned, spent: spent, remains: remainsToSpent, free: freeAbs)
    }
    
    private func calculateCategoryStatistics(period: Period) -> [CategoryStatistics] {
        var categoryStatistics = [CategoryStatistics]()
        let groupedPeriodOutcomes = Dictionary(grouping: outcomes, by: { $0.category_id })
        let groupedMthOutcomes = Dictionary(grouping: mthOutcomes, by: { $0.category_id })
        
        coreDataManager.getObjects(of: Category.self).forEach { category in
            let categoryPeriodOutcomes = groupedPeriodOutcomes[category.id] ?? []
            let reducedPeriod = categoryPeriodOutcomes.reduce(0, { $0 + $1.spent })
            
            let categoryMthOutcomes = groupedMthOutcomes[category.id] ?? []
            let reducedMth = categoryMthOutcomes.reduce(0, { $0 + $1.spent })
            
            let statistics = CategoryStatistics(of: category, mthSpent: reducedMth, for: period, periodSpent: reducedPeriod)
            categoryStatistics.append(statistics)
        }
        
        return categoryStatistics
    }
}


extension MainPresenter: IMainPresenter {
    
    func periodTapped(periodValue: Period.Value) {
        period = Period(value: periodValue, isChoosen: true)
        viewDidLoad()
    }
    
    func categoryTapped(at index: Int?) {
        if index == nil {
            wireframe.presentCategoryEditorView(category: nil, delegate: self)
        } else {
            guard let categoryStat = categoryList.first(where: { $0.id == Int16(categoryList[index!].id) }) else { return }
            wireframe.moveToCategoryView(category: categoryStat.category, period: self.period, delegate: self)
        }
    }
    
    func cardTapped(at index: Int?) {
        if index == nil {
            wireframe.presentCardEditorView(card: nil, delegate: self)
        } else {
            guard let index = index, index < cardList.count else { return }
            wireframe.moveToCardView(card: cardList[index], period: period, delegate: self)
        }
    }
    
    func addOutcome(at index: Int, for card: Card) {
        if let category = coreDataManager.getObject(of: Category.self, id: Int16(categoryList[index].id)) {
            wireframe.presentOutcomeEditorView(card: card, category: category, delegate: self)
        }
    }
    
    func transfer(to index: Int?, from card: Card) {
        if let index = index, index < cardList.count {
            let cardTo = cardList[index]
            wireframe.presentTransferEditorView(from: card, to: cardTo, delegate: self)
        }
    }
    
    func setCodeTapped() {
        wireframe.presentAuthView(delegate: self)
    }
    
    func moveCategoryToIndex(_ category: Category, to index: Int) {
        if category.position < index {
            let leadingCategories = coreDataManager.getObjects(of: Category.self).filter { $0.position <= index && $0.position > category.position }
            leadingCategories.forEach { $0.position -= 1 }
        } else {
            let tailingCategories = coreDataManager.getObjects(of: Category.self).filter { $0.position >= index }
            tailingCategories.forEach { $0.position += 1 }
        }
        category.position = Int16(index)
        coreDataManager.saveChanges()
    }
    
    func hideInactiveCategories(isHide: Bool) {
        hideInactiveCategories = isHide
        view?.reloadCategories()
    }
    
    func statisticsTapped() {
        wireframe.moveToStatisticsView(period: period, delegate: self)
    }
}

extension MainPresenter: ICategoryEditorOutput, ICategoryOutput {
    func categoryDeleted() {
        view?.reloadCategories()
        view?.setStatistics(calculatePeriodStatistics(period: period))
    }
    
    func categoryEdited() {
        view?.reloadCategories()
        view?.setStatistics(calculatePeriodStatistics(period: period))
    }
}

extension MainPresenter: ICardEditorOutput, ICardOutput {
    func cardEdited() {
        view?.reloadCards()
        view?.setStatistics(calculatePeriodStatistics(period: period))
    }
    
    func cardDeleted() {
        view?.reloadCards()
        view?.setStatistics(calculatePeriodStatistics(period: period))
    }
}

extension MainPresenter: IOutcomeEditorOutput {
    func outcomeEdited() {
        viewDidLoad()
    }
}

extension MainPresenter: ITransferEditorOutput {
    func transferEdited() {
        view?.reloadCards()
    }
}

extension MainPresenter: IAuthDelegate {
    func lockStateUpdated() {
        view?.setLockImage()
    }
}
