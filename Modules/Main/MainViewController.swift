//
//  MainViewController.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 29.07.2021.
//

import UIKit

protocol IMainViewController: IBaseViewController {
    var presenter: IMainPresenter? { get set }
    
    func setPeriod(_ period: Period)
    func setStatistics(_ statistics: PeriodStatistics)
    func setSwitcherOn(_ isOn: Bool)
    func reloadCards()
    func reloadCategories()
    func setLockImage()
}


final class MainViewController: BaseViewController {
    
    var presenter: IMainPresenter?
    
    private lazy var headerView: IHeaderView = {
        let view = HeaderView(frame: .zero)
        view.periodTapped = { [weak self] period in
            guard let period = period else { return }
            self?.presenter?.periodTapped(periodValue: period)
        }
        return view
    }()
    private lazy var cardsCollectionView: ICardsViewController = {
        let vc = CardsViewController()
        vc.presenter = self.presenter
        return vc
    }()
    private lazy var switcher: UISwitch = {
        let switcher = UISwitch()
        switcher.isOn = true
        switcher.onTintColor = Theme.current.separatorColor.withAlphaComponent(0.7)
        switcher.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        switcher.addTarget(self, action: #selector(switchChanged), for: .valueChanged)
        return switcher
    }()
    private lazy var switchActives: UIView = {
        let view = UIView()
        view.backgroundColor = Theme.current.mainBackgroundColor
        
        view.addSubview(switcher)
        switcher.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.trailing.equalToSuperview().offset(-Const.mainOffset)
        }
        view.addSubview(switchActivesTitle)
        switchActivesTitle.snp.makeConstraints { make in
            make.trailing.equalTo(switcher.snp.leading).offset(-Const.mainOffset)
            make.centerY.equalToSuperview()
        }
        
        return view
    }()
    private lazy var switchActivesTitle: UILabel = {
        let label = UILabel()
        label.font = Const.textFont
        label.textColor = Theme.current.mainTextColor.withAlphaComponent(0.7)
        label.textAlignment = .right
        label.text = "Hide inactive categories".localized()
        return label
    }()
    private lazy var categoriesViewController: ICategoriesViewController = {
        let vc = CategoriesViewController()
        vc.presenter = self.presenter
        return vc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func setupViews() {
        let lockImage = (presenter?.isLockSet ?? false) ? UIImage(systemName: "lock.fill") : UIImage(systemName: "lock.open.fill")
        let leftButton = UIBarButtonItem(image: lockImage, style: .plain, target: self, action: #selector(settingsTapped))
        navigationItem.leftBarButtonItem = leftButton
        
        let rightButton = UIBarButtonItem(image: UIImage(systemName: "chart.bar.xaxis"), style: .plain, target: self, action: #selector(statisticsTapped))
        navigationItem.rightBarButtonItem = rightButton
        
        view.backgroundColor = Theme.current.secondBackgroundColor

        view.addSubview(headerView)
        
        addChild(cardsCollectionView)
        view.addSubview(cardsCollectionView.view)
        cardsCollectionView.didMove(toParent: self)
        
        view.addSubview(switchActives)
        
        addChild(categoriesViewController)
        view.addSubview(categoriesViewController.view)
        categoriesViewController.didMove(toParent: self)

        setupTransitionConstraints()
    }
    
    override func setupAppearance() {
        switchActives.backgroundColor = Theme.current.mainBackgroundColor
        switchActivesTitle.textColor = Theme.current.mainTextColor.withAlphaComponent(0.7)
        view.backgroundColor = Theme.current.secondBackgroundColor
        navigationController?.navigationBar.reloadInputViews()
    }
    
    override func setupTransitionConstraints() {
        let orientation = UIDevice.current.orientation
        if orientation.isPortrait {
            headerView.snp.remakeConstraints { remake in
                remake.top.equalTo(view).offset(safeNavBarHeight)
                remake.leading.trailing.equalToSuperview()
            }
            cardsCollectionView.view.snp.remakeConstraints { remake in
                remake.top.equalTo(headerView.snp.bottom)
                remake.leading.trailing.equalToSuperview()
                remake.height.equalTo(Const.cardsCvHeight)
            }
            switchActives.snp.remakeConstraints { remake in
                remake.top.equalTo(cardsCollectionView.view.snp.bottom)
                remake.leading.trailing.equalToSuperview()
            }
            categoriesViewController.view.snp.remakeConstraints { remake in
                remake.top.equalTo(switchActives.snp.bottom)
                remake.leading.trailing.equalToSuperview()
                remake.bottom.equalToSuperview()
            }
        } else if orientation.isLandscape {
            headerView.snp.remakeConstraints { remake in
                let screenWidth = min(view.frame.width, view.frame.height)
                remake.top.equalTo(view).offset(navBarHeight)
                remake.leading.equalToSuperview()
                remake.width.equalTo(screenWidth)
            }
            cardsCollectionView.view.snp.remakeConstraints { remake in
                remake.top.equalTo(headerView.snp.bottom)
                remake.leading.equalToSuperview()
                remake.trailing.equalTo(headerView)
                remake.height.equalTo(Const.cardsCvHeight)
            }
            switchActives.snp.remakeConstraints { remake in
                remake.top.equalTo(view).offset(navBarHeight)
                remake.leading.equalTo(headerView.snp.trailing)
                remake.trailing.equalToSuperview()
            }
            categoriesViewController.view.snp.remakeConstraints { remake in
                remake.top.equalTo(switchActives.snp.bottom)
                remake.leading.equalTo(headerView.snp.trailing)
                remake.bottom.trailing.equalToSuperview()
            }
        }
    }
    
    @objc private func settingsTapped() {
        presenter?.setCodeTapped()
    }
    
    @objc private func statisticsTapped() {
        presenter?.statisticsTapped()
    }
    
    @objc private func switchChanged() {
        presenter?.hideInactiveCategories(isHide: switcher.isOn)
    }
}


extension MainViewController: IMainViewController {
    func setPeriod(_ period: Period) {
        title = period.value.mainTitle
        headerView.setPeriod(period)
    }
    
    func setStatistics(_ statistics: PeriodStatistics) {
        headerView.setStatistics(statistics)
    }
    
    func setSwitcherOn(_ isOn: Bool) {
        switcher.isOn = isOn
    }
    
    func reloadCards() {
        cardsCollectionView.reloadData()
    }
    
    func reloadCategories() {
        categoriesViewController.reloadData()
    }
    
    func setLockImage() {
        if let leftButton = navigationItem.leftBarButtonItem {
            leftButton.image = (presenter?.isLockSet ?? false) ? UIImage(systemName: "lock.fill") : UIImage(systemName: "lock.open.fill")
        }
    }
}
