//
//  MainWireframe.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 29.07.2021.
//

import UIKit

protocol IMainWireframe: IBaseWireframe {
    func presentCategoryEditorView(category: Category?, delegate: ICategoryEditorOutput)
    func presentCardEditorView(card: Card?, delegate: ICardEditorOutput)
    func presentOutcomeEditorView(card: Card, category: Category, delegate: IOutcomeEditorOutput)
    func presentTransferEditorView(from card1: Card, to card2: Card, delegate: ITransferEditorOutput)
    func presentAuthView(delegate: IAuthDelegate?)
    
    func moveToCategoryView(category: Category, period: Period, delegate: ICategoryOutput)
    func moveToCardView(card: Card, period: Period, delegate: ICardCategoryOutput)
    func moveToStatisticsView(period: Period, delegate: ICardCategoryOutput?)
}

final class MainWireframe: IMainWireframe {
    weak var view: IBaseViewController?
    
    init(view: IBaseViewController) {
        self.view = view
    }
    
    static func buildModule() -> UIViewController {
        let view: IMainViewController = MainViewController()
        let wireframe: IMainWireframe = MainWireframe(view: view)
        let presenter: IMainPresenter = MainPresenter(wireframe: wireframe, view: view)
        view.presenter = presenter
        return view
    }
    
    func presentCategoryEditorView(category: Category?, delegate: ICategoryEditorOutput) {
        let vc = CategoryEditorWireframe.buildModule(category: category, delegate: delegate) as UIViewController
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        view?.present(vc, animated: true, completion: nil)
    }
    
    func presentCardEditorView(card: Card?, delegate: ICardEditorOutput) {
        let vc = CardEditorWireframe.buildModule(card: card, delegate: delegate) as UIViewController
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        view?.present(vc, animated: true, completion: nil)
    }
    
    func presentOutcomeEditorView(card: Card, category: Category, delegate: IOutcomeEditorOutput) {
        let outcome: Outcome? = nil
        guard let vc = OutcomeEditorWireframe.buildModule(card: card, category: category, outcome: outcome, delegate: delegate) else {
            print("Cannot resolve OutcomeEditor module!")
            return
        }
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        view?.present(vc, animated: true, completion: nil)
    }
    
    func presentTransferEditorView(from card1: Card, to card2: Card, delegate: ITransferEditorOutput) {
        let vc = TransferEditorWireframe.buildModule(from: card1, to: card2, delegate: delegate)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        view?.present(vc, animated: true, completion: nil)
    }
    
    func presentAuthView(delegate: IAuthDelegate?) {
        let vc = AuthWireframe.buildModule(delegate: delegate)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        view?.present(vc, animated: true, completion: nil)
    }
    
    func moveToCategoryView(category: Category, period: Period, delegate: ICategoryOutput) {
        let vc = CategoryWireframe.buildModule(category: category, delegate: delegate, period: period)
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func moveToCardView(card: Card, period: Period, delegate: ICardCategoryOutput) {
        let vc = CardWireframe.buildModule(card: card, period: period, delegate: delegate)
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func moveToStatisticsView(period: Period, delegate: ICardCategoryOutput?) {
        let vc = StatisticsWireframe.buildModule(period: period, delegate: delegate)
        view?.navigationController?.pushViewController(vc, animated: true)
    }
}
