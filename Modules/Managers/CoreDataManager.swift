//
//  CoreDataManager.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 29.07.2021.
//

import Foundation
import CoreData

protocol ICoreDataManager: AnyObject {
    func getObject<T: NSManagedObject>(of type: T.Type, id: Int16) -> T?
    func getObjects<T: NSManagedObject>(of type: T.Type) -> [T]
    func getOutcomes(for period: Period) -> [Outcome]
    func getOutcomes(startDate: Date, endDate: Date) -> [Outcome]
    func getTransfers(for period: Period) -> [Transfer]
    func createObject<T: NSManagedObject>(of type: T.Type) -> T
    func removeObject(_ object: NSManagedObject)
    func clearDatabase()
    func saveChanges()
}


final class CoreDataManager: ICoreDataManager {
    
    private static var instance: ICoreDataManager?
    
    static var shared: ICoreDataManager {
        if let instance = Self.instance {
            return instance
        } else {
            instance = CoreDataManager()
            return Self.instance!
        }
    }
    
    var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Money_Hustle")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    lazy var context: NSManagedObjectContext = {
        return persistentContainer.viewContext
    }()
    
    func getObject<T: NSManagedObject>(of type: T.Type, id: Int16) -> T? {
        let fetchRequest: NSFetchRequest<T> = NSFetchRequest<T>(entityName: T.description())
        fetchRequest.predicate = NSPredicate(format: "id = %i", id)
        let objects = try? context.fetch(fetchRequest)
        return objects?.first
    }
    
    func getObjects<T: NSManagedObject>(of type: T.Type) -> [T] {
        let fetchRequest: NSFetchRequest<T> = NSFetchRequest<T>(entityName: T.description())
        var objects = try? context.fetch(fetchRequest)
        if type == Category.self || type == Card.self{
            let filtered = objects?.filter { object in
                if let object = object as? Category {
                    return !object.isArchived
                } else if let object = object as? Card {
                    return !object.isArchived
                } else {
                    return true
                }
            }
            objects = filtered
        }
        return objects ?? []
    }
    
    func getOutcomes(for period: Period) -> [Outcome] {
        let startDate = period.getStartDate(offset: 0)
        let endDate = period.getEndDate(offset: 0)
        
        let nsStartDate = NSDate(timeIntervalSince1970: startDate.timeIntervalSince1970)
        let nsEndDate = NSDate(timeIntervalSince1970: endDate.timeIntervalSince1970)
        let fetchRequest: NSFetchRequest<Outcome> = NSFetchRequest<Outcome>(entityName: Outcome.description())
        fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date < %@", nsStartDate, nsEndDate)
        let objects = try? context.fetch(fetchRequest)
        return objects ?? []
    }
    
    func getOutcomes(startDate: Date, endDate: Date) -> [Outcome] {
        let nsStartDate = NSDate(timeIntervalSince1970: startDate.timeIntervalSince1970)
        let nsEndDate = NSDate(timeIntervalSince1970: endDate.timeIntervalSince1970)
        let fetchRequest: NSFetchRequest<Outcome> = NSFetchRequest<Outcome>(entityName: Outcome.description())
        fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date < %@", nsStartDate, nsEndDate)
        let objects = try? context.fetch(fetchRequest)
        return objects ?? []
    }
    
    func getTransfers(for period: Period) -> [Transfer] {
        let startDate = period.getStartDate(offset: 0)
        let endDate = period.getEndDate(offset: 0)
        
        let nsStartDate = NSDate(timeIntervalSince1970: startDate.timeIntervalSince1970)
        let nsEndDate = NSDate(timeIntervalSince1970: endDate.timeIntervalSince1970)
        let fetchRequest: NSFetchRequest<Transfer> = NSFetchRequest<Transfer>(entityName: Transfer.description())
        fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date < %@", nsStartDate, nsEndDate)
        let objects = try? context.fetch(fetchRequest)
        return objects ?? []
    }
    
    func createObject<T: NSManagedObject>(of type: T.Type) -> T {
        T(context: context)
    }
    
    func removeObject(_ object: NSManagedObject) {
        context.delete(object)
    }
    
    func saveChanges() {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                context.rollback()
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func clearDatabase() {
        
    }
}
