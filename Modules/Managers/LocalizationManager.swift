//
//  LocalizationManager.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 03.11.2021.
//

import Foundation

final class LocalizationManager {
    
    var locale: String
    
    private static let currentLocalizations = ["en", "ru"]
    private static var instance: LocalizationManager?
    
    static var shared: LocalizationManager {
        if let instance = Self.instance {
            return instance
        } else {
            instance = LocalizationManager()
            return Self.instance!
        }
    }
    
    init() {
        var deviceLanguage = NSLocale.preferredLanguages.first?.prefix(2) ?? "en"
        if !LocalizationManager.currentLocalizations.contains(String(deviceLanguage)) {
            deviceLanguage = "en"
        }
        locale = String(deviceLanguage)
    }
}
