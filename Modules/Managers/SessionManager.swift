//
//  SessionManager.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 31.08.2021.
//

import Foundation

protocol ISessionManager: AnyObject {
    var isAuth: Bool { get }
    var isCodeSet: Bool { get }
    
    func setEnterCode(code: String)
    func isCorrectCode(enteredCode: String) -> Bool
    func deleteEnterCode()
    func safeEntrance()
    func closeSession()
}


final class SessionManager: ISessionManager {
    var isAuth: Bool
    var isCodeSet: Bool {
        let defaults = UserDefaults.standard
        let value = defaults.string(forKey: "Code")
        
        if #available(iOS 14.0, *) {
            return !(value?.isEmpty ?? true)
        } else {
            return false
        }

    }
    
    private static var instance: ISessionManager?
    
    static var shared: ISessionManager {
        if let instance = Self.instance {
            return instance
        } else {
            instance = SessionManager()
            return Self.instance!
        }
    }
    
    init() {
        isAuth = false
    }
    
    func setEnterCode(code: String) {
        let defaults = UserDefaults.standard
        defaults.setValue(code, forKey: "Code")
        isAuth = true
    }
    
    func isCorrectCode(enteredCode: String) -> Bool {
        let defaults = UserDefaults.standard
        guard let value = defaults.string(forKey: "Code") else {return false }
        isAuth = value == enteredCode
        return value == enteredCode
    }
    
    func deleteEnterCode() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "Code")
    }
    
    func safeEntrance() {
        isAuth = true
    }
    
    func closeSession() {
        isAuth = false
    }
}
