//
//  OutcomeEditorPresenter.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 13.08.2021.
//

import Foundation

protocol IOutcomeEditorOutput: AnyObject {
    func outcomeEdited()
}

protocol IOutcomeEditorPresenter: IBasePresenter {
    func viewBackTapped()
    func saveTapped(outcomeString: String, commentString: String?, date: Date)
    func deleteTapped()
}


final class OutcomeEditorPresenter: IOutcomeEditorPresenter {
    
    private let wireframe: IOutcomeEditorWireframe
    private weak var view: IOutcomeEditorViewController?
    private weak var delegate: IOutcomeEditorOutput?
    
    private let card: Card?
    private let category: Category?
    private let outcome: Outcome?
    
    init(wireframe: IOutcomeEditorWireframe, view: IOutcomeEditorViewController, delegate: IOutcomeEditorOutput?, card: Card?, category: Category?, outcome: Outcome?) {
        self.wireframe = wireframe
        self.view = view
        self.delegate = delegate
        self.card = card
        self.category = category
        self.outcome = outcome
    }
    
    func viewDidLoad() {
        if let outcome = outcome {
            view?.setOutcome(outcome)
        }
    }
    
    func viewBackTapped() {
        wireframe.closeModule()
    }
    
    func saveTapped(outcomeString: String, commentString: String?, date: Date) {
        if let spent = Double(outcomeString) {
            if let card = card, let category = category {
                let newOutcome = coreDataManager.createObject(of: Outcome.self)
                newOutcome.configure(card: card, category: category, spent: spent, comment: commentString, date: date)
                
                let cardToModify = coreDataManager.getObject(of: Card.self, id: card.id)
                cardToModify?.amount = card.amount - spent
            } else if let outcome = self.outcome {
                let outcomeToModify = coreDataManager.getObject(of: Outcome.self, id: outcome.id)
                
                if let cardId = outcomeToModify?.card_id, let cardToModify = coreDataManager.getObject(of: Card.self, id: cardId) {
                    cardToModify.amount = cardToModify.amount + outcome.spent - spent
                }
                
                outcomeToModify?.spent = spent
                outcomeToModify?.comment = commentString
                outcomeToModify?.date = date
            }
            
            
            coreDataManager.saveChanges()
            delegate?.outcomeEdited()
            wireframe.closeModule()
        }
    }
    
    func deleteTapped() {
        if let outcome = outcome {
            if let cardToModify = coreDataManager.getObject(of: Card.self, id: outcome.card_id) {
                cardToModify.amount = cardToModify.amount + outcome.spent
            }
            coreDataManager.removeObject(outcome)
            coreDataManager.saveChanges()
            delegate?.outcomeEdited()
            wireframe.closeModule()
        }
    }
}
