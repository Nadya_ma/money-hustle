//
//  OutcomeEditorViewController.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 13.08.2021.
//

import UIKit

protocol IOutcomeEditorViewController: IBaseViewController {
    var presenter: IOutcomeEditorPresenter? { get set }
    
    func setOutcome(_ outcome: Outcome?)
}


final class OutcomeEditorViewController: BaseViewController {
    var presenter: IOutcomeEditorPresenter?
    
    private lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.alwaysBounceVertical = true
        view.alwaysBounceHorizontal = false
        view.backgroundColor = .clear
        view.showsVerticalScrollIndicator = false
        return view
    }()
    private lazy var mainContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    private let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = Const.cornerRadius
        view.isUserInteractionEnabled = true
        return view
    }()
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Add outcome".localized()
        return label
    }()
    private lazy var datePickerView: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.maximumDate = Date().endOfDay
        datePicker.locale = Locale(identifier: LocalizationManager.shared.locale)
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .compact
        }
        return datePicker
    }()
    private let outcomeField: CommonAmountTextField = {
        let field = CommonAmountTextField()
        field.title = "Outcome amount".localized()
        return field
    }()
    private let outcomeCommentField: CommonTextField = {
        let field = CommonTextField()
        field.title = "Comment".localized()
        field.keyBoardType = .default
        return field
    }()
    private let saveButton: UIButton = {
        var button = UIButton()
        button.setTitle("Add".localized(), for: .normal)
        button.layer.cornerRadius = Const.cornerRadius
        button.addTarget(self, action: #selector(saveTapped), for: .touchUpInside)
        return button
    }()
    private let deleteButton: UIButton = {
        var button = UIButton()
        button.setTitle("Delete".localized(), for: .normal)
        button.addTarget(self, action: #selector(deleteTapped), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subscribeToShowKeyboardNotifications()
        presenter?.viewDidLoad()
    }
    
    override func setupViews() {
        view.backgroundColor = .black.withAlphaComponent(0.3)
        setupAppearance()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:))))
        
        let containerWidth = min(view.frame.width, view.frame.height)
        
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        scrollView.addSubview(mainContainerView)
        mainContainerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalTo(view.snp.width)
        }

        mainContainerView.addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.width.equalTo(containerWidth * 0.67)
            make.top.equalToSuperview().offset(120)
            make.centerX.equalToSuperview()
        }
        
        containerView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview().inset(Const.largeOffset)
        }
        
        containerView.addSubview(datePickerView)
        datePickerView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(Const.largeOffset)
            make.centerX.equalToSuperview()
        }
        
        containerView.addSubview(outcomeField)
        outcomeField.snp.makeConstraints { make in
            make.top.equalTo(datePickerView.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.equalToSuperview().inset(Const.largeOffset)
            make.height.equalTo(2*Const.iconCvItemSize.height)
        }
        
        containerView.addSubview(outcomeCommentField)
        outcomeCommentField.snp.makeConstraints { make in
            make.top.equalTo(outcomeField.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.equalToSuperview().inset(Const.largeOffset)
            make.height.equalTo(2*Const.iconCvItemSize.height)
        }
        
        containerView.addSubview(saveButton)
        saveButton.snp.makeConstraints { make in
            make.top.equalTo(outcomeCommentField.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.bottom.equalToSuperview().inset(Const.largeOffset)
            make.height.equalTo(Const.buttonHeight)
        }
        
        mainContainerView.addSubview(deleteButton)
        deleteButton.snp.makeConstraints { make in
            make.top.equalTo(containerView.snp.bottom).offset(Const.largeOffset)
            make.leading.trailing.equalTo(containerView)
            make.height.equalTo(Const.buttonHeight)
            make.bottom.equalToSuperview()
        }
    }
    
    override func setupAppearance() {
        containerView.backgroundColor = Theme.current.mainBackgroundColor
        titleLabel.textColor = Theme.current.mainTextColor
        datePickerView.tintColor = Theme.current.separatorColor
        saveButton.backgroundColor = Theme.current.separatorColor
        deleteButton.setTitleColor(Theme.current.minusTextColor, for: .normal)
    }
    
    @objc func viewTapped(_ gr: UITapGestureRecognizer) {
        let point = gr.location(in: gr.view)
        if !containerView.frame.contains(point) && !deleteButton.frame.contains(point) {
            presenter?.viewBackTapped()
        }
    }
    
    @objc func saveTapped() {
        guard let outcome = outcomeField.value else { return }
        let comment = outcomeCommentField.value
        let date = datePickerView.date
        presenter?.saveTapped(outcomeString: outcome, commentString: comment, date: date)
    }
    
    @objc func deleteTapped() {
        presenter?.deleteTapped()
    }
}

extension OutcomeEditorViewController: IOutcomeEditorViewController {
    func setOutcome(_ outcome: Outcome?) {
        if let outcome = outcome {
            titleLabel.text = "Outcome info".localized()
            datePickerView.date = outcome.date
            outcomeField.value = String(outcome.spent)
            outcomeCommentField.value = outcome.comment
            deleteButton.isHidden = false
        }
    }
}

extension OutcomeEditorViewController {
    private func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height + 142 // 128 xs
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        scrollView.contentInset.bottom = 0
    }
}
