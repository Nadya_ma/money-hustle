//
//  OutcomeEditorWireframe.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 13.08.2021.
//

import UIKit

protocol IOutcomeEditorWireframe: IBaseWireframe {
    func closeModule()
}


final class OutcomeEditorWireframe: IOutcomeEditorWireframe {
    weak var view: IBaseViewController?
    
    init(view: IBaseViewController) {
        self.view = view
    }
    
    static func buildModule(card: Card?, category: Category?, outcome: Outcome?, delegate: IOutcomeEditorOutput) -> UIViewController? {
        if outcome != nil || (card != nil && category != nil) {
            let view: IOutcomeEditorViewController = OutcomeEditorViewController()
            let wireframe: IOutcomeEditorWireframe = OutcomeEditorWireframe(view: view)
            let presenter: IOutcomeEditorPresenter = OutcomeEditorPresenter(wireframe: wireframe, view: view, delegate: delegate, card: card, category: category, outcome: outcome)
            view.presenter = presenter
            return view
        } else {
            return nil
        }
    }
    
    func closeModule() {
        view?.dismiss(animated: true, completion: nil)
    }
}
