//
//  StatisticsCell.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 15.11.2021.
//

import SnapKit


final class StatisticsCell: UITableViewCell {
    static let id = "StatisticsCell"
    
    private let detailsLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = Const.boldTextFont
        return label
    }()
    private let averAmountLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = Const.addDetailsFont
        return label
    }()
    private let averAmountArrow: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        return view
    }()
    private let amountLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = Const.boldTextFont
        return label
    }()
    private let amountArrow: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        return view
    }()
    private let dividerView: UIView = {
        let view = UIView()
        view.backgroundColor = Theme.current.separatorColor.withAlphaComponent(0.3)
        return view
    }()
    
    var statisticsOutcome: StatisticsOutcome? {
        didSet {
            if let outcome = statisticsOutcome {
                detailsLabel.text = outcome.category.name
                amountLabel.text = outcome.amount.stringValue
                averAmountLabel.text = "In average".localized() + ": " + outcome.averAmount.stringValue
                if outcome.lastAmount != 0 && outcome.amount != outcome.lastAmount {
                    amountArrow.image = UIImage(systemName: outcome.amount < outcome.lastAmount ? "chevron.up" : "chevron.down")
                    amountArrow.tintColor = outcome.amount < outcome.lastAmount ? Theme.current.minusTextColor : Theme.current.plusTextColor
                }
                if outcome.averLastAmount != 0 && outcome.averAmount != outcome.averLastAmount {
                    averAmountArrow.image = UIImage(systemName: outcome.averLastAmount < outcome.averAmount ? "chevron.up" : "chevron.down")
                    averAmountArrow.tintColor = outcome.averLastAmount < outcome.averAmount ? Theme.current.minusTextColor : Theme.current.plusTextColor
                }
            }
        }
    }
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        setupAppearance()
        
        contentView.addSubview(amountArrow)
        amountArrow.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(Const.largeOffset)
            make.centerY.equalToSuperview()
            make.size.equalTo(12)
        }

        contentView.addSubview(amountLabel)
        amountLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(amountArrow.snp.leading).offset(-Const.mainOffset)
        }
        
        contentView.addSubview(detailsLabel)
        detailsLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(Const.mainOffset)
            make.leading.equalToSuperview().offset(Const.largeOffset)
            make.trailing.equalTo(amountLabel.snp.leading)
        }
        
        contentView.addSubview(averAmountLabel)
        averAmountLabel.snp.makeConstraints { make in
            make.top.equalTo(detailsLabel.snp.bottom).offset(Const.mainOffset / 2)
            make.leading.equalToSuperview().offset(Const.largeOffset)
        }
        
        contentView.addSubview(averAmountArrow)
        averAmountArrow.snp.makeConstraints { make in
            make.centerY.equalTo(averAmountLabel)
            make.leading.equalTo(averAmountLabel.snp.trailing).offset(0.7 * Const.mainOffset)
            make.size.equalTo(8)
        }
        
        contentView.addSubview(dividerView)
        dividerView.snp.makeConstraints { make in
            make.top.equalTo(averAmountLabel.snp.bottom).offset(Const.mainOffset)
            make.leading.equalToSuperview().inset(Const.largeOffset)
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        setupAppearance()
    }
    
    private func setupAppearance() {
        contentView.backgroundColor = Theme.current.mainBackgroundColor
        detailsLabel.textColor = Theme.current.mainTextColor
        averAmountLabel.text = nil
        averAmountArrow.image = nil
        amountLabel.textColor = Theme.current.mainTextColor
        amountArrow.image = nil
    }
}
