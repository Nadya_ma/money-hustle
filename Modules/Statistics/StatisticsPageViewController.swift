//
//  StatisticsPageViewController.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 13.09.2021.
//

import UIKit

protocol IStatisticsPageViewController: IBaseViewController {
    var presenter: IStatisticsPresenter? { get set }
    
    func setPeriodDates(start: Date, end: Date, periodOffset: Int)
    func setOutcomes(_ outcomes: [StatisticsOutcome])
}


final class StatisticsPageViewController: BaseViewController {
    var presenter: IStatisticsPresenter?
    private var periodOffset: Int = 0
    private var outcomes = [StatisticsOutcome]()
    
    private lazy var outcomesTable: UITableView = {
        let tv = UITableView()
        tv.register(StatisticsCell.self, forCellReuseIdentifier: StatisticsCell.id)
        tv.separatorStyle = .none
        tv.estimatedRowHeight = 200
        tv.dataSource = self
        tv.delegate = self
        return tv
    }()
    private lazy var datesLabel: UILabel = {
        let label = UILabel()
        label.font = Const.h1BoldTitleFont
        label.textAlignment = .center
        return label
    }()
    
    
    override func setupViews() {
        setupAppearance()
        
        view.addSubview(outcomesTable)
        outcomesTable.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(Const.mainOffset)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    override func setupAppearance() {
        view.backgroundColor = Theme.current.secondBackgroundColor
        datesLabel.textColor = Theme.current.mainTextColor
        outcomesTable.backgroundColor = Theme.current.secondBackgroundColor
        outcomesTable.reloadData()
    }
}


extension StatisticsPageViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        outcomes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: StatisticsCell.id, for: indexPath)
        (cell as? StatisticsCell)?.statisticsOutcome = outcomes[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let mainView = UIView()
        mainView.backgroundColor = Theme.current.separatorColor.withAlphaComponent(0.5)
        
        let balanceTitleLabel = UILabel()
        balanceTitleLabel.font = Const.descriptionFont
        balanceTitleLabel.textColor = Theme.current.mainTextColor
        balanceTitleLabel.textAlignment = .center
        balanceTitleLabel.text = "Dates".localized()
    
        mainView.addSubview(balanceTitleLabel)
        balanceTitleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(Const.largeOffset)
            make.centerX.equalToSuperview()
        }
        
        mainView.addSubview(datesLabel)
        datesLabel.snp.makeConstraints { make in
            make.top.equalTo(balanceTitleLabel.snp.bottom)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-Const.largeOffset)
        }
        return mainView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = Theme.current.secondBackgroundColor
        
        let finalLabel = UILabel()
        finalLabel.textAlignment = .right
        finalLabel.font = Const.boldTextFont
        finalLabel.textColor = Theme.current.mainTextColor
        
        let finalAmount = outcomes.reduce(0) { $0 + ($1.amount < 0 ? $1.amount : 0) }
        finalLabel.text = "Total spent".localized() + ": " + finalAmount.stringValue
        
        view.addSubview(finalLabel)
        finalLabel.snp.makeConstraints { make in
            make.top.trailing.bottom.equalToSuperview().inset(Const.largeOffset)
        }
        
        return view
    }
}

extension StatisticsPageViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < outcomes.count {
            presenter?.onCategoryTapped(outcomes[indexPath.row].category, periodOffset: periodOffset)
        }
    }
}

extension StatisticsPageViewController: IStatisticsPageViewController {
    func setPeriodDates(start: Date, end: Date, periodOffset: Int) {
        self.periodOffset = periodOffset
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: LocalizationManager.shared.locale)
        dateFormatter.dateFormat = "d MMM yy"
        datesLabel.text = dateFormatter.string(from: start) + " - " +  dateFormatter.string(from: end)
    }
    
    func setOutcomes(_ outcomes: [StatisticsOutcome]) {
        self.outcomes = outcomes
        outcomesTable.reloadData()
    }
}

