//
//  StatisticsPresenter.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 13.09.2021.
//

import UIKit

protocol IStatisticsPresenter: IBasePresenter {
    var pageVCs: [IBaseViewController] { get set }
    
    func periodTapped(_ period: Period.Value)
    func onCategoryTapped(_ category: Category, periodOffset: Int)
}


final class StatisticsPresenter: IStatisticsPresenter {
    private let wireframe: IStatisticsWireframe
    private weak var delegate: ICardCategoryOutput?
    private weak var view: IStatisticsViewController?

    private var previousPeriod: Period?
    private var period: Period
    
    var pageVCs: [IBaseViewController] = []
    
    init(wireframe: IStatisticsWireframe, delegate: ICardCategoryOutput?, view: IStatisticsViewController, period: Period) {
        self.wireframe = wireframe
        self.delegate = delegate
        self.view = view
        self.period = period
    }
    
    func viewDidLoad() {
        setVCs()
        view?.setPeriod(period)
        if let previousPeriod = previousPeriod {
            view?.reloadData(in: period.value > previousPeriod.value ? .forward : .reverse)
        } else {
            view?.reloadData(in: .forward)
        }
    }
    
    
    private func setVCs() {
        var offset = 1
        var allOutcomes = [Int: [StatisticsOutcome]]()
        while true {
            guard offset <= 12 else { break }
            let start = period.getStartDate(offset: offset)
            let end = period.getEndDate(offset: offset)
            let outcomes = calculateOutcomes(startDate: start, endDate: end)
            if outcomes.isEmpty { break }
            allOutcomes[offset] = outcomes
            offset += 1
        }
        
        offset = 1
        while true {
            guard let currentOutcomes = allOutcomes[offset] else { break }
            
            if let prevOutcomes = allOutcomes[offset+1] {
                currentOutcomes.enumerated().forEach { (id, outcome) in
                    if let prevOutcome = prevOutcomes.first(where: { $0.category == outcome.category }) {
                        allOutcomes[offset]?[id].lastAmount = prevOutcome.amount
                        allOutcomes[offset]?[id].averLastAmount = prevOutcome.averAmount
                    }
                }
            }
            offset += 1
        }
        
        offset = 1
        var vcs = [IBaseViewController]()
        while true {
            guard let outcomes = allOutcomes[offset] else { break }
            print(outcomes)
            let start = period.getStartDate(offset: offset)
            let end = period.getEndDate(offset: offset)
            
            let vc = wireframe.resolveStatisticsPageView()
            vc.setPeriodDates(start: start, end: end, periodOffset: offset)
            vc.setOutcomes(outcomes)
            vc.presenter = self
            vcs.append(vc)
            offset += 1
        }
        self.pageVCs = vcs
    }
    
    private func calculateOutcomes(startDate: Date, endDate: Date) -> [StatisticsOutcome] {
        var outcomes = [StatisticsOutcome]()
        coreDataManager.getObjects(of: Category.self).forEach { category in
            let categoryOutcomes = coreDataManager.getOutcomes(startDate: startDate, endDate: endDate).filter { $0.category_id == category.id }
            let amount = categoryOutcomes.reduce(0.0) { $0 + $1.spent }
            if amount > 0 {
                outcomes.append(StatisticsOutcome(category: category, card: nil, amount: -amount, averAmount: amount/Double(categoryOutcomes.count)))
            }
        }
        return outcomes.sorted { $0.amount < $1.amount }
    }
    
    func periodTapped(_ period: Period.Value) {
        if self.period.value != period {
            self.previousPeriod = self.period
            self.period = Period(value: period, isChoosen: true)
            viewDidLoad()
        }
    }
    
    func onCategoryTapped(_ category: Category, periodOffset: Int) {
        wireframe.moveToCategoryView(category: category, period: period, periodOffset: periodOffset, delegate: self)
    }
}


extension StatisticsPresenter: ICategoryOutput {
    func categoryEdited() {
        delegate?.categoryEdited()
    }
    
    func categoryDeleted() {
        delegate?.categoryDeleted()
    }
    
    func outcomeEdited() {
        viewDidLoad()
        delegate?.outcomeEdited()
    }
}
