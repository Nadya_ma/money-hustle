//
//  StatisticsViewController.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 14.09.2021.
//

import UIKit

protocol IStatisticsViewController: IBaseViewController {
    var presenter: IStatisticsPresenter? { get set }
    
    func setPeriod(_ period: Period)
    func reloadData(in direction: UIPageViewController.NavigationDirection)
}


final class StatisticsViewController: BaseViewController {
    var presenter: IStatisticsPresenter?
    
    private let periodsStack: UIStackView = {
        let stack = UIStackView()
        stack.spacing = Const.stackSpacing
        stack.distribution = .fillEqually
        stack.axis = .horizontal
        return stack
    }()
    private lazy var pageVC: UIPageViewController = {
        let pvc = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pvc.dataSource = self
        pvc.delegate = self
        return pvc
    }()
    private let pageControl = UIPageControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func setupViews() {
        setupAppearance()
        
        view.addSubview(periodsStack)
        view.addSubview(pageControl)
        view.addSubview(pageVC.view)
        setupTransitionConstraints()
    }
    
    override func setupAppearance() {
        view.backgroundColor = Theme.current.secondBackgroundColor
        pageControl.pageIndicatorTintColor = Theme.current.mainBackgroundColor
        pageControl.currentPageIndicatorTintColor = Theme.current.separatorColor
    }
    
    override func setupTransitionConstraints() {
        let orientation = UIDevice.current.orientation
        periodsStack.snp.remakeConstraints { remake in
            remake.top.equalToSuperview().offset(orientation.isLandscape ? navBarHeight + 12 : safeNavBarHeight + 20)
            remake.leading.trailing.equalToSuperview().inset(Const.largeOffset)
        }
        pageControl.snp.remakeConstraints { remake in
            remake.leading.trailing.bottom.equalToSuperview().inset(Const.largeOffset)
        }
        pageVC.view.snp.remakeConstraints { remake in
            remake.top.equalTo(periodsStack.snp.bottom)
            remake.leading.trailing.equalToSuperview()
            remake.bottom.equalTo(pageControl.snp.top)
        }
    }
}

extension StatisticsViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? IBaseViewController,
              let presenter = presenter,
              let index = presenter.pageVCs.firstIndex(where: { $0 == vc }),
              index > 0 else { return nil }
        return presenter.pageVCs[index-1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? IBaseViewController,
              let presenter = presenter,
              let index = presenter.pageVCs.firstIndex(where: { $0 == vc }),
              index < presenter.pageVCs.count - 1 else { return nil }
        return presenter.pageVCs[index+1]
    }
}

extension StatisticsViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        guard let vc = pendingViewControllers.first as? IBaseViewController,
              let presenter = presenter,
              let index = presenter.pageVCs.firstIndex(where: { $0 == vc }),
              index < presenter.pageVCs.count else { return }
        pageControl.currentPage = index
    }
}

extension StatisticsViewController: IStatisticsViewController {
    func setPeriod(_ period: Period) {
        let allCases = Period.Value.allCases
        let index = allCases.firstIndex(of: period.value) ?? 0
        
        periodsStack.arrangedSubviews.forEach { $0.removeFromSuperview() }
        allCases.enumerated().forEach { (id, value) in
            let view = PeriodCell()
            view.tag = id
            view.period = value
            view.isChoosen = id == index
            view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(periodTapped(_:))))
            periodsStack.addArrangedSubview(view)
        }
        
        let backButton = UIBarButtonItem()
        backButton.title = period.value.mainTitle
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    @objc private func periodTapped(_ gr: UITapGestureRecognizer) {
        if let index = gr.view?.tag, let period = Period.Value.init(intValue: index) {
            presenter?.periodTapped(period)
        }
    }
    
    func reloadData(in direction: UIPageViewController.NavigationDirection) {
        if let presenter = presenter {
            pageControl.numberOfPages = presenter.pageVCs.count
            pageControl.currentPage = 0
            guard let viewController = presenter.pageVCs.first else { return }
            pageVC.setViewControllers([viewController], direction: direction, animated: true, completion: nil)
        }
    }
}
