//
//  StatisticsWireframe.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 13.09.2021.
//

import UIKit

protocol IStatisticsWireframe: IBaseWireframe {
    func resolveStatisticsPageView() -> IStatisticsPageViewController
    func moveToCategoryView(category: Category, period: Period, periodOffset: Int, delegate: ICategoryOutput)
    func closeModule()
}


final class StatisticsWireframe: IStatisticsWireframe {
    var view: IBaseViewController?
    
    init(view: IBaseViewController) {
        self.view = view
    }
    
    static func buildModule(period: Period, delegate: ICardCategoryOutput?) -> UIViewController {
        let view: IStatisticsViewController = StatisticsViewController()
        let wireframe: IStatisticsWireframe = StatisticsWireframe(view: view)
        let presenter: IStatisticsPresenter = StatisticsPresenter(wireframe: wireframe, delegate: delegate, view: view, period: period)
        view.presenter = presenter
        return view
    }
    
    func resolveStatisticsPageView() -> IStatisticsPageViewController {
        let view: IStatisticsPageViewController = StatisticsPageViewController()
        return view
    }
    
    func moveToCategoryView(category: Category, period: Period, periodOffset: Int, delegate: ICategoryOutput) {
        let vc = CategoryWireframe.buildModule(category: category, delegate: delegate, period: period, periodOffset: periodOffset)
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func closeModule() {
        view?.navigationController?.popViewController(animated: true)
    }
}
