//
//  CardOutcome.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 23.08.2021.
//

import Foundation

struct CardOutcome {
    
    enum OutcomeType {
        case transfer
        case outcome
        case changeSum
    }
    
    var date: Date
    var type: OutcomeType
    var category: Category?
    var transfer: Transfer? = nil
    var card: Card?
    var amount: Double
    
    var comment: String {
        switch type {
        case .transfer  : return (amount > 0 ? "Transfer from card".localized() : "Transfer to card".localized()) + " \(card?.name ?? "")"
        case .outcome   : return "\(category?.name ?? "Spendings for category".localized())"
        case .changeSum : return (amount > 0 ? "Card amount increased for".localized() : "Card amount decreased for".localized()) + " \(amount)"
        }
    }
}
