//
//  CategoryStatistics.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 09.08.2021.
//

import UIKit

struct CategoryStatistics {
    var id: Int
    var name: String
    var image: UIImage?
    var color: UIColor?
    var planned: Double
    var spent: Double
    var remains: Double
    var category: Category
    var period: Period
    
    init(of category: Category, mthSpent: Double, for period: Period, periodSpent: Double) {
        self.id = Int(category.id)
        self.name = category.name
        if Int(category.image_id) < categoryIcons.count {
            self.image = categoryIcons[Int(category.image_id)].icon
            self.color = categoryIcons[Int(category.image_id)].color
        }
        
        spent = periodSpent
        
        let mthPlanRemains = category.mth_plan > mthSpent - periodSpent ? category.mth_plan - mthSpent + periodSpent : 0
        let weightedPeriodPlan = mthPlanRemains * period.weight
        var periodPlanRemains: Double = 0
        periodPlanRemains = periodSpent < weightedPeriodPlan ? weightedPeriodPlan : ( periodSpent < mthPlanRemains ? periodSpent : mthPlanRemains )
        
        planned = period.value == .month ? category.mth_plan : periodPlanRemains
        remains = planned - periodSpent
        
        self.category = category
        self.period = period
    }
}
