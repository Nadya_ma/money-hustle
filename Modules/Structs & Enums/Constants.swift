//
//  Const.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 16.08.2021.
//

import UIKit

struct Const {
    static let mainOffset: CGFloat = 8
    static let largeOffset: CGFloat = 16
    static let cardsCvHeight: CGFloat = 100
    static let cvSpacing: CGFloat = 12
    static let tvItemEstimatedHeight: CGFloat = 56
    static let buttonHeight: CGFloat = 48
    
    static let stackSpacing: CGFloat = 8
    static let cornerRadius: CGFloat = 12
    static let bigCornerRadius: CGFloat = 16
    static let borderWidth: CGFloat = 1
    
    static let addDetailsLabelHeight: CGFloat = 12
    
    static let cardsCvItemSize = CGSize(width: 80, height: 80)
    static let categoryCvItemSize = CGSize(width: 80, height: 120)
    static let iconCvItemSize = CGSize(width: 50, height: 50)
    static let markImageRadius: CGFloat = 24
    
    static let addDetailsFont = UIFont.systemFont(ofSize: 10)
    static let boldAddDetailsFont = UIFont.boldSystemFont(ofSize: 10)
    static let italicAddDetailsFont = UIFont.italicSystemFont(ofSize: 10)
    static let descriptionFont = UIFont.systemFont(ofSize: 12)
    static let boldDescriptionFont = UIFont.boldSystemFont(ofSize: 12)
    static let textFont = UIFont.systemFont(ofSize: 14)
    static let boldTextFont = UIFont.boldSystemFont(ofSize: 14)
    static let h4TitleFont = UIFont.systemFont(ofSize: 18)
    static let h4BoldTitleFont = UIFont.boldSystemFont(ofSize: 18)
    static let h1BoldTitleFont = UIFont.boldSystemFont(ofSize: 24)
}

let categoryIcons: [(icon: UIImage?, color: UIColor)] = [(UIImage(systemName: "cart.fill"), #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)),
                                                         (UIImage(systemName: "person.2.fill"), #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)),
                                                         (UIImage(systemName: "leaf.fill"), #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)),
                                                         (UIImage(systemName: "heart.fill"), #colorLiteral(red: 1, green: 0.07517843984, blue: 0.3643311648, alpha: 1)),
                                                         (UIImage(systemName: "car.fill"), #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)),
                                                         (UIImage(systemName: "house.fill"), #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)),
                                                         (UIImage(systemName: "gift.fill"), #colorLiteral(red: 1, green: 0.2527923882, blue: 1, alpha: 1)),
                                                         (UIImage(systemName: "hammer.fill"), #colorLiteral(red: 0.5787474513, green: 0.3215198815, blue: 0, alpha: 1)),
                                                         (UIImage(systemName: "bag.fill"), #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)),
                                                         (UIImage(systemName: "trash.fill"), #colorLiteral(red: 0.4508578777, green: 0.9882974029, blue: 0.8376303315, alpha: 1)),
                                                         (UIImage(systemName: "crown.fill"), #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)),
                                                         (UIImage(systemName: "archivebox.fill"), #colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 1)),
                                                         (UIImage(systemName: "star.fill"), #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1)),
                                                         (UIImage(systemName: "tag.fill"), #colorLiteral(red: 1, green: 0.4932718873, blue: 0.4739984274, alpha: 1)),
                                                         (UIImage(systemName: "mouth.fill"), #colorLiteral(red: 0.5807225108, green: 0.066734083, blue: 0, alpha: 1)),
                                                         (UIImage(systemName: "face.smiling.fill"), #colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1)),
                                                         (UIImage(systemName: "figure.walk"), #colorLiteral(red: 0.476841867, green: 0.5048075914, blue: 1, alpha: 1)),
                                                         (UIImage(systemName: "cross.fill"), #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1))]
                                                        // (UIImage(named: "t_shirt"), #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1))

let cardColors: [UIColor] = [#colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1), #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1), #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.4513868093, green: 0.9930960536, blue: 1, alpha: 1), #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1), #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1), #colorLiteral(red: 0.8446564078, green: 0.5145705342, blue: 1, alpha: 1)]
