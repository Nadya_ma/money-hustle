//
//  Period.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 02.08.2021.
//

import Foundation

struct Period {
    
    let value: Value
    var isChoosen: Bool
    
    enum Value: String, Comparable, CaseIterable {
        case day
        case week
        case month
        
        init?(intValue: Int) {
            switch intValue {
            case 0: self = .day
            case 1: self = .week
            case 2: self = .month
            default: return nil
            }
        }
        
        var title: String {
            switch self {
            case .day   : return "Day".localized()
            case .week  : return "Week".localized()
            case .month : return "Month".localized()
            }
        }
        
        var planTitle: String {
            switch self {
            case .day   : return "Day plan".localized()
            case .week  : return "Week plan".localized()
            case .month : return "Month plan".localized()
            }
        }
        
        var mainTitle: String {
            switch self {
            case .day   : return "Dayly Money Hustle".localized()
            case .week  : return "Weekly Money Hustle".localized()
            case .month : return "Monthly Money Hustle".localized()
            }
        }
        
        private var intValue: Int {
            switch self {
            case .day   : return 0
            case .week  : return 1
            case .month : return 2
            }
        }
        
        static func < (lhs: Period.Value, rhs: Period.Value) -> Bool {
            lhs.intValue < rhs.intValue
        }
    }
    
    init(value: Value, isChoosen: Bool) {
        self.value = value
        self.isChoosen = isChoosen
    }
    
    var weight: Double {
        switch self.value {
        case .day:
            let start = Date().startOfMonth
            let monthDayCount = 1 + Calendar.current.component(.day, from: Date().endOfMonth) - Calendar.current.component(.day, from: start)
            let currenDay = Calendar.current.component(.day, from: Date()) - 1
            return 1 / Double(monthDayCount - currenDay)
        case .month: return 1
        case .week:
            let start = Date().startOfWeekOrMonth
            let end = Date().endOfWeekOrMonth
            let monthDayCount = 1 + Calendar.current.component(.day, from: Date().endOfMonth) - Calendar.current.component(.day, from: start)
            let weekDayCount = 1 + Calendar.current.component(.day, from: end) - Calendar.current.component(.day, from: start)
            return Double(weekDayCount) / Double(monthDayCount)
        }
    }
    
    func getStartDate(offset: Int) -> Date {
        var components = DateComponents()
        switch self.value {
        case .day   :
            components.day = -offset
            let date = Calendar.current.date(byAdding: components, to: Date()) ?? Date()
            return date.startOfDay
        case .week  :
            components.weekOfYear = -offset
            let date = Calendar.current.date(byAdding: components, to: Date()) ?? Date()
            return date.startOfWeek
        case .month :
            components.month = -offset
            let date = Calendar.current.date(byAdding: components, to: Date()) ?? Date()
            return date.startOfMonth
        }
    }
    
    func getEndDate(offset: Int) -> Date {
        var components = DateComponents()
        switch self.value {
        case .day   :
            components.day = -offset
            let date = Calendar.current.date(byAdding: components, to: Date()) ?? Date()
            return date.endOfDay
        case .week  :
            components.weekOfYear = -offset
            let date = Calendar.current.date(byAdding: components, to: Date()) ?? Date()
            return offset == 0 ? date.endOfWeekOrMonth : date.endOfWeek
        case .month :
            components.month = -offset
            let date = Calendar.current.date(byAdding: components, to: Date()) ?? Date()
            return date.endOfMonth
        }
    }
}
