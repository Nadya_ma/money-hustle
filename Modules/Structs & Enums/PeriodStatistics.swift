//
//  PeriodStatistics.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 02.08.2021.
//

import Foundation

struct PeriodStatistics {
    var planned: Double
    var spent: Double
    var remains: Double
    var free: Double
}
