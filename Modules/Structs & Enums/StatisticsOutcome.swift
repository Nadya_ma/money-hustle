//
//  StatisticsOutcome.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 15.11.2021.
//

import Foundation

struct StatisticsOutcome {
    
    var category: Category
    var card: Card?
    var amount: Double
    var averAmount: Double
    var lastAmount: Double = 0
    var averLastAmount: Double = 0
    
    var description: String {
        "category: \(self.category.name), amount: \(self.amount), lastAmount: \(self.lastAmount)"
    }
}
