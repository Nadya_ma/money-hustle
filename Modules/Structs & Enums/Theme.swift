//
//  Theme.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 16.08.2021.
//

import UIKit

enum Theme: String {
    case light
    case dark
}

extension Theme {
    static var current: Theme {
        get {
            let defaults = UserDefaults.standard
            let value = defaults.string(forKey: "Theme")
            if let value = value, let themeValue = Theme(rawValue: value) {
                return themeValue
            } else {
                defaults.set(Theme.light.rawValue, forKey: "Theme")
                return Theme.light
            }
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue.rawValue, forKey: "Theme")
        }
    }
    
    var mainBackgroundColor: UIColor {
        switch self {
        case .light: return UIColor.white
        case .dark: return UIColor.systemGray5
        }
    }
    
    var secondBackgroundColor: UIColor {
        switch self {
        case .light: return UIColor.systemGray6
        case .dark: return UIColor.systemGray3
        }
    }
    
    var separatorColor: UIColor {
        switch self {
        case .light: return UIColor.systemTeal
        case .dark: return UIColor.systemBlue
        }
    }
    
    var buttonBackgroundColor: UIColor {
        switch self {
        case .light: return  UIColor.systemTeal
        case .dark: return UIColor.systemBlue
        }
    }
    
    var mainTextColor: UIColor {
        switch self {
        case .light: return UIColor.black
        case .dark: return UIColor.white
        }
    }
    
    var minusTextColor: UIColor {
        switch self {
        case .light: return UIColor.red
        case .dark: return UIColor.red
        }
    }
    
    var plusTextColor: UIColor {
        switch self {
        case .light: return UIColor.green
        case .dark: return UIColor.green
        }
    }
    
    var secondSeparatorColor: UIColor {
        return UIColor.magenta.withAlphaComponent(0.7)
    }
}
