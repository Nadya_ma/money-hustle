//
//  TransferEditorPresenter.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 17.08.2021.
//

import Foundation

protocol ITransferEditorOutput: AnyObject {
    func transferEdited()
}

protocol ITransferEditorPresenter: IBasePresenter {
    func viewBackTapped()
    func saveTapped(outcomeString: String, date: Date)
    func deleteTapped()
}


final class TransferEditorPresenter: ITransferEditorPresenter {
    
    private let wireframe: ITransferEditorWireframe
    private weak var view: ITransferEditorViewController?
    private weak var delegate: ITransferEditorOutput?
    
    private let cardFrom: Card?
    private let cardTo: Card?
    private let transfer: Transfer?
    
    init(wireframe: ITransferEditorWireframe, view: ITransferEditorViewController, delegate: ITransferEditorOutput?, from card1: Card?, to card2: Card?, transfer: Transfer?) {
        self.wireframe = wireframe
        self.view = view
        self.delegate = delegate
        self.cardFrom = card1
        self.cardTo = card2
        self.transfer = transfer
    }
    
    func viewDidLoad() {
        if let transfer = transfer {
            view?.setTransfer(transfer)
        }
    }
    
    func viewBackTapped() {
        wireframe.closeModule()
    }
    
    func saveTapped(outcomeString: String, date: Date) {
        guard let transferAmount = Double(outcomeString), transferAmount != 0 else { return }
            
        if let cardFrom = cardFrom, let cardTo = cardTo {
            cardFrom.amount = cardFrom.amount - transferAmount
            cardTo.amount = cardTo.amount + transferAmount
            
            let transfer = coreDataManager.createObject(of: Transfer.self)
            transfer.configure(fromCard: cardFrom, toCard: cardTo, amount: transferAmount, date: date)
        }
        
        coreDataManager.saveChanges()
        delegate?.transferEdited()
        wireframe.closeModule()
    }
    
    func deleteTapped() {
        if let transfer = transfer {
            let cardFrom = coreDataManager.getObject(of: Card.self, id: transfer.from_card_id)
            let cardTo = coreDataManager.getObject(of: Card.self, id: transfer.to_card_id)
            
            cardTo?.amount -= transfer.amount
            if cardFrom != cardTo {
                cardFrom?.amount += transfer.amount
            }
            
            coreDataManager.removeObject(transfer)
            coreDataManager.saveChanges()
            delegate?.transferEdited()
            wireframe.closeModule()
        }
    }
}
