//
//  TransferEditorWireframe.swift
//  Money Hustle
//
//  Created by Nadya Mamysheva on 17.08.2021.
//

import UIKit

protocol ITransferEditorWireframe: IBaseWireframe {
    func closeModule()
}


final class TransferEditorWireframe: ITransferEditorWireframe {
    weak var view: IBaseViewController?
    
    init(view: IBaseViewController) {
        self.view = view
    }
    
    static func buildModule(from card1: Card?, to card2: Card?, delegate: ITransferEditorOutput?, transfer: Transfer? = nil) -> UIViewController {
        let view: ITransferEditorViewController = TransferEditorViewController()
        let wireframe: ITransferEditorWireframe = TransferEditorWireframe(view: view)
        let presenter: ITransferEditorPresenter = TransferEditorPresenter(wireframe: wireframe, view: view, delegate: delegate, from: card1, to: card2, transfer: transfer)
        view.presenter = presenter
        return view
    }
    
    func closeModule() {
        view?.dismiss(animated: true, completion: nil)
    }
}
