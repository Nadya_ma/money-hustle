# money-hustle

Application for controlling your money flow. 

Features: 
1) Supports both orientations
2) Supports dark and light mode
3) FaceId/TouchId
4) Localized in ru/en

Technologies:
1) Swift
2) UI written by code
3) MVP
4) CoreData
5) Pods: Snapkit, UPCarouselFlowLayout, KDCircularProgress

Video-demo:
https://drive.google.com/file/d/18Yh17cF4XwWBWTDGkHNP2IwOpoprnMXa/view?usp=share_link
